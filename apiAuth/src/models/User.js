//const Absences = require("../apiAbsence/models/Absences.model");
const mongoose = require('mongoose'), Schema = mongoose.Schema;

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: "Name is required!",
    },
    email: {
      type: String, 
      min: (5, 'Too short, min is 5 characters'),
      max: (32, 'Too long, max is 32 characters'),
      unique: true,
      lowercase: true,
      //match: (/^w+((.-)?w+)*@w+((.-)?w+)*(.w{2,3})+$/),
      required: "Email is required!",
    },
    password: {
      type: String,
      required: "Password is required!",
    },
    role: {
      type: String,
      default: 'admin',
      enum: ["admin", "school manager", "finance manager","teacher","parent","student"]
     },
     registrationNumber: {
      type: Number,
      required: "registrationNumber is required!",
    },
     birthdate : {
      type: Date,
      required: "birthdate is required!",
    },
     phoneNumber: {
      type: Number,
      required: "phoneNumber is required!",
    },
     address: {
      type: String,
      required: false,
    },
     salary: {
      type: Number,
      required: "salary is required!",
    },
     gender: {
      type: String,
      enum: ["male", "female"]
     },
     category: {
      type: String,
      default: 'staff',
      enum: ["staff", "manager"]
     },
     contractDueDate: {
      type: Date,
      required: false,
    },
     contractType: {
      type: String,
      default: 'cdi',
      enum: ["cdi", "cdd"]
     },
     nic: {
      type: String,
      required: false,
    },
    status : {
      type: String,
      required: false,
    },
     photo : {
      type: String,
      required: false,
    } ,
    school_level: { type: Number, required: false },
    last_school: { type: String, required: false },
    study_fees: { type: Number, required: false },
    photo: { type: String, required: false },
    schoolYears: { type: String, required: false },
    quarters: { type: String, required: false },
    classes: { type: String, required: false },
    schedules: { type: String, required: false },
    sessions: { type: String, required: false },
    subjects: { type: String, required: false },
    tests: { type: String, required: false },
    /*absences:[{
      type:Schema.Types.ObjectId,
      ref:"Absences"
  }],*/
  /*parents:[{
    type:Schema.Types.ObjectId,
    ref:"User"
}],*/
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("User", userSchema);



