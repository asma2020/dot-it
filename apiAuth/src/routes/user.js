const router = require("express").Router();
const { catchErrors } = require("../handlers/errorHandlers");
const userController = require("../controllers/userController");

router.post("/login", userController.login);
router.post("/register", userController.register);
router.get("/users", userController.users);
router.get("/user-profile/:userid", userController.profile);
router.put("/update-user/:userid", userController.update);
router.delete("/delete-user/:userid", userController.delete);
router.post('/token/reject', userController.rejectToken);
router.post('/token/refresh', userController.token);

/************* */
//get all users absence 
//router.get("/:userid/getAbsence", catchErrors(userController.getUserAbsence));
//post a new classe to a quarter   
//router.post("/:userid/postAbsence", catchErrors(userController.newUserAbsence));

module.exports = router;




