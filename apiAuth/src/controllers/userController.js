const mongoose = require("mongoose");
const User = mongoose.model("User");
const sha256 = require("js-sha256");
const jwt = require("jwt-then");
//const Absence = require("./../apiAbsence/models/Absences.model");
var randtoken = require('rand-token');
var refreshTokens = {};
const axios = require("axios");

exports.register = async (req, res) => {
  const { name, email, password, role, registrationNumber, birthdate, phoneNumber, address, salary, gender, category, contractDueDate, contractType, nic, status, photo, school_level, last_school, study_fees, schoolYears, quarters, classes, schedules, sessions, subjects, tests } = req.body;

  /*const emailRegex = /@gmail.com|@yahoo.com|@hotmail.com|@live.com/;

  if (!emailRegex.test(email)) throw "Email is not supported from your domain.";
  if (password.length < 6) throw "Password must be atleast 6 characters long.";*/

  const userExists = await User.findOne({
    email,
  });

  if (userExists) throw "User with same email already exits.";

  const user = new User({
    name,
    email,
    password: sha256(password + process.env.SALT),
    role,
    registrationNumber,
    birthdate,
    phoneNumber,
    address,
    salary,
    gender,
    category,
    contractDueDate,
    contractType,
    nic,
    status,
    photo,
    school_level,
    last_school,
    study_fees,
    schoolYears,
    quarters,
    classes,
    schedules,
    sessions,
    subjects,
    tests,
  });

  const userSave=await user.save();
  const _id=userSave._id;
  console.log("id user ",userSave._id)
  await axios.post('http://localhost:4005/events', {
    type: 'Registration',
    data: {
      _id,
      name,
      email,
      password,
      role,
      registrationNumber,
      birthdate,
      phoneNumber,
      address,
      salary,
      gender,
      category,
      contractDueDate,
      contractType,
      nic,
      status,
      photo,
      school_level,
      last_school,
      study_fees,
      schoolYears,
      quarters,
      classes,
      schedules,
      sessions,
      subjects,
      tests,
    },
  });

  res.json({
    message: "User [" + name + "] registered successfully!",
  });
};


exports.login = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({
    email,
    password: sha256(password + process.env.SALT),
  });

  if (!user) throw "Email and Password did not match.";

  var token = await jwt.sign({ id: user.id }, process.env.SECRET, {
    expiresIn: 3000,
  });

  var refreshToken = randtoken.uid(256);
  refreshTokens[refreshToken] = email;
 //Publishing this event to the event-bus project (event broker)

  await axios.post('http://localhost:4005/events', {
    type: 'Authentification',
    data: {
      email,
      password,
      token,
    },
  });

  res.json({
    message: 'User logged in successfully!',
    status: 200,
    data: { access_token: token, refreshToken: refreshToken, user: user },
  });
};



/**
 * DESTROY TOKEN / LOGOUT
 * @param {refreshToken} req
 * @param {status} res
 */
exports.rejectToken = async (req, res) => {
  var refreshToken = req.body.refreshToken;
  if (refreshToken in refreshTokens) {
    delete refreshTokens[refreshToken];
  }
  res.send({ msg: 'logout', status: 204 });
};

exports.token = async (req, res) => {
  var email = req.body.email;
  var refreshToken = req.body.refreshToken;
  if (refreshToken in refreshTokens && refreshTokens[refreshToken] == email) {
    var token = await jwt.sign({ id: req.body.id }, process.env.SECRET, {
      expiresIn: 3000,
    });
    res.json({ token: token });
  } else {
    res.send(401);
  }
};


// Get Users
exports.users = async (req, res) => {
  User.find((error, response) => {
    if (error) {
      return next(error)
    } else {
      res.status(200).json(response)
    }
  })
};

// Get Single User
exports.profile = async (req, res) => {
  User.findById(req.params.userid, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
};

// Update User
exports.update = async (req, res) => {
  User.findByIdAndUpdate(req.params.userid, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error)
    } else {
      res.json(data)
      console.log('User successfully updated!')
    }
  })
};


// Delete User
exports.delete = async (req, res) => {
  User.findByIdAndRemove(req.params.userid, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
};


/************************* */
/*exports.getUserAbsence = async (req, res, next) => {
  try {
    const { userid } = req.params;
    const user = await User.findById(userid).populate('absences');
    res.status(200).json(user.absences)
  } catch (err) {
    next(err);
  }
};*/


/*exports.newUserAbsence= async(req, res, next) => {
  try {
  const {userId} = req.params;
  const newUserAbsence = new UserAbsence({
    _id: new mongoose.Types.ObjectId(),
    designation: req.body.designation,
    hours_number: req.body.hours_number,
    justification:req.body.justification,
  });
  //get user
  const user = await UserAbsence.findById(userId);
  //assign userAbsence to user
  newUserAbsence.userAbsences = user;
  //save the event
   await newUserAbsence.save();
  // add UserAbsence to users
  user.userAbsences.push(newUserAbsence);
  //save the user
   await user.save();
 
  res.status(200).json(newUserAbsence);
} catch (err) {
  next(err);
}
};*/

/*exports.newUserAbsence = async (req, res, next) => {
  try {
    const { userId } = req.params;
    //const id = req.params.schoolYearId;
    const newAbsence = new Absence({
      _id: new mongoose.Types.ObjectId(),
      designation: req.body.designation,
      hours_number: req.body.hours_number,
      justification: req.body.justification,

    });
    //get user
    const user = await User.findById(userid);
    //assign a to user
    newAbsence.absences = user;
    //save the absence
    await newAbsence.save();
    // add absence to user's
    user.absences.push(newAbsence);
    //save the absence
    await user.save();

    res.status(200).json(newAbsence);
  } catch (err) {
    next(err);
  }
};*/