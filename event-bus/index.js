const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');

const app = express();
app.use(bodyParser.json());

//A LOCAL DATABASE/STORAGE TO THE EVENTS
const events = [];

app.post('/events', (req, res) => {
  


  const event = req.body;

  console.log("event bus ",event)
  //Storing the sent event in the events array
  ///if(events.includes)
  events.push(event);

  /*Deffusing the different events coming from different services between each other*/
  //LOGIN SERVICE EVENTS
  axios.post('http://localhost:7001/events', event);
   
  res.send({ status: 'OK' });
});

//Getting the list/array of all the events published
app.get('/events', (req, res) => {
  res.send(events);

});

app.listen(4005, () => {
  console.log('Listening on 4005');
});
