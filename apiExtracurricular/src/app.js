const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const morgan = require("morgan");

//Setup Cross Origin
app.use(require("cors")());
//Upload Image
app.use(bodyParser.json())
app.use(morgan("dev"));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

//Bring in the apiExtracurricular routes
app.use("/comments",require("./apiExtracurricular/routes/comment.routes.js"));
app.use("/extracurriculars",require("./apiExtracurricular/routes/extracurricular.routes.js"));

module.exports = app;
