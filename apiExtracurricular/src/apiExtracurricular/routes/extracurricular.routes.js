const router = require("express").Router();
const mongoose = require("mongoose");
const multer = require('multer');
const extracurricularController = require("../controllers/extracurricular");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    //cb(null, new Date().toISOString() + file.originalname);
    cb(null, Date.now() + file.originalname);  
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
    storage: storage,
    limits: {
     fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
  });

router.get("/get", extracurricularController.getall);
router.post("/post",upload.single('photo'), extracurricularController.post);
router.get("get/:extracurricularId",extracurricularController.getone);
router.patch("/:extracurricularId",upload.single('photo'), extracurricularController.update);
router.delete("/:extracurricularId", extracurricularController.delete);


/********************************** */
router.get("/getAllExtracurricular", extracurricularController.getAllExtracurricular);
//router.post("/getUser", extracurricularController.getUser);
router.post("/newExtracurricular",upload.single('photo'),extracurricularController.newExtracurricular);
router.get("/getoneExtracurricular/:extracurricularId", extracurricularController.getoneExtracurricular);
router.patch("/replaceExtracurricular/:extracurricularId",upload.single('photo'), extracurricularController.replaceExtracurricular);
router.put("/updateExtracurricular/:extracurricularId",upload.single('photo'),extracurricularController.updateExtracurricular );
router.delete("/deleteExtracurricular/:extracurricularId", extracurricularController.deleteExtracurricular );
/************* */
//get all extracurricular classes
router.get("/:extracurricularId/getComment", extracurricularController.getExtracurricularComment);
//post a new Comment to a Extracurricular  
router.post("/:extracurricularId/postComment", extracurricularController.newExtracurricularComment);
// add like
router.patch("/:extracurricularId/addLike", extracurricularController.updateLike);
//add dislike
router.patch("/:extracurricularId/addDislike", extracurricularController.updateDislike);
// get statistique 
router.get("/statistique", extracurricularController.statistique );


module.exports = router;
