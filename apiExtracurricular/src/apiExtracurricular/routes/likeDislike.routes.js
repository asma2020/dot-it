module.exports = (app) => {
    const likes = require('../controllers/likeDislike.controller.js');

    // Add like
    app.patch('/add/dislike/:extracurricularId', likes.update );

}
