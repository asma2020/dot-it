const router = require("express").Router();
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
const commentController = require('../controllers/comment.controller.js');

router.get("/get", commentController.findAll);
router.post("/post",commentController.create);
router.get("get/:commentId",commentController.findOne);
router.patch("/:commentId",commentController.update);
router.delete("/:commentId", commentController.delete);

/************* */
router.get("/getAllExtracurricular", commentController.getAllExtracurricular);
router.post("/newComment", commentController.newComment);
router.get("/getCommentExtracurricular/:commentId",commentController.getonecommentextracurricular);
router.patch("/replaceCommentExtracurricular/:commentId", commentController.replacecommentextracurricular );
router.put("/updateCommentExtracurricular/:commentId", commentController.updatecommentextracurricular );
router.delete("/deleteCommentExtracurricular/:commentId", commentController.deletecommentextracurricular );
/************* */
module.exports = router;
