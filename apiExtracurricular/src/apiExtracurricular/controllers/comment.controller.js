const Comment = require('../models/comment.model.js');
const Extracurricular  = require("../models/extracurricular.model");
var mongoose = require('mongoose');


// Create and Save a new Extracurricular

exports.create = (req, res) => {
    // Validate request
    if(!req.body.content) {
        return res.status(400).send({
            message: "Comment content can not be empty"
        });
    }

    // Create a Comment
    const comment = new Comment({
        content:req.body.content,
    });

    // Save Comment in the database
    comment.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Comment."
        });
    });
};

// Retrieve and return all comments from the database.
exports.findAll = (req, res) => {
    Comment.find()
    .then(comments => {
        res.send(comments);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving comments."
        });
    });
};

// Find a single comment with a commentrId
exports.findOne = (req, res) => {
    Comment.findById(req.params.commentId)
    .then(comment => {
        if(!comment) {
            return res.status(404).send({
                message: "Comment not found with id " + req.params.commentId
            });            
        }
        res.send(comment);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Comment not found with id " + req.params.commentId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving comment with id " + req.params.commentId
        });
    });
};

// Update a comment identified by the commentId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body.content) {
        return res.status(400).send({
            message: "Comment content can not be empty"
        });
    }

    // Find comment and update it with the request body
    Comment.findByIdAndUpdate(req.params.commentId, {
        content:req.body.content,
    }, {new: true})
    .then(comment => {
        if(!comment) {
            return res.status(404).send({
                message: "Comment not found with id " + req.params.commentId
            });
        }
        res.send(comment);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Comment not found with id " + req.params.commentId
            });                
        }
        return res.status(500).send({
            message: "Error updating comment with id " + req.params.commentId
        });
    });
};

// Delete a comment with the specified commentId in the request
exports.delete = (req, res) => {
    Comment.findByIdAndRemove(req.params.commentId)
    .then(comment => {
        if(!extracurricular) {
            return res.status(404).send({
                message: "Comment not found with id " + req.params.commentId
            });
        }
        res.send({message: "Comment deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Comment not found with id " + req.params.commentId
            });                
        }
        return res.status(500).send({
            message: "Could not delete comment with id " + req.params.commentId
        });
    });
};
/*****************************************/
exports.getAllExtracurricular= async (req, res, next) => {
    try {
      const comments = await Comment.find({});
      res.status(200).json(comments);
    } catch (err) {
      next(err);
    }
  
  };
  
  exports.newComment = async (req, res, next) => {
    try {
      // find actuel extracurricular
      const extracurriculars = await Extracurricular.findById(req.body.extracurriculars);
      //create a new Comment
      const newComment = {
        _id: new mongoose.Types.ObjectId(),
        content:req.body.content,
      };
  
      delete newComment.extracurriculars;
      const comment = new Comment(newComment);
      comment.extracurriculars = extracurriculars;
      await comment.save();
      extracurriculars.comments.push(comment);
      await extracurriculars.save();
      res.status(200).json(comment);
  
    } catch (err) {
      next(err);
    }
  
  };
  
  exports.getonecommentextracurricular = async (req, res, next) => {
    try {
     const comment= await Extracurricular.findById(req.params.commentId);
     res.status(200).json(comment);
    } catch (err) {
      next(err);
    }
  
  };
  
  exports.replacecommentextracurricular = async (req, res, next) => {
    try {
      const result = await Extracurricular.findByIdAndUpdate(req.params.commentId, {
        content: req.body.content,
      });  
     res.status(200).json({succes:true});
    } catch (err) {
      next(err);
    }
  
  };
  
  exports.updatecommentextracurricular = async (req, res, next) => {
    try {
     const result = await Comment.findByIdAndUpdate(req.params.commentId, {
        content: req.body.content,
    });
  
     res.status(200).json({succes:true});
    } catch (err) {
      next(err);
    }
  
  };
  
  exports.deletecommentextracurricular = async (req, res, next) => {
    try {
      const commentId = req.params.commentId;
     //get comment
     const comment= await Comment.findById(commentId);
     if (!comment){
       return res.status(404).json({error : 'we can not found this comment'});
     }
     console.log(comment);
     const extracurricularId = comment.extracurriculars;
     console.log(extracurricularId);
     // get extracurricular
     const extracurriculars = await Extracurricular.findById(extracurricularId);
     console.log(extracurriculars);
     //remove comment
     await comment.remove();
     console.log(comment);
     console.log(extracurriculars);
     //remove from extracurricular list
     extracurriculars.comments.pull(comment);
     await extracurriculars.save();
  
     res.status(200).json({succes:true});
    } catch (err) {
      next(err);
    }
  
  };
