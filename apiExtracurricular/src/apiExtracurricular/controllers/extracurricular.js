const express = require('express');
//const router = express.Router();
const mongoose = require("mongoose");
const User = require("../models/User"); 
const Extracurricular = require("../models/extracurricular.model");
const Comment = require("../models/comment.model");
//const Like = require("../models/Like.model");
//const Dislike = require("../models/Dislike.model");
/*const multer = require('multer');

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};*/

/*
    if (path.extension(file.originalname) !== '.pdf') {
      return cb(new Error('Only pdfs are allowed'))
    }

    cb(null, true)
  }
;*/

/*const upload = multer({
  storage: storage,
  limits: {
   fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});*/

//router.get("/get", (req, res, next) => {
  exports.getall = (req, res) => {
  Extracurricular.find()
    .select("libelle publisher photo  file  video content level subject likes_count dislikes_count")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        extracurriculars: docs.map(doc => {
          return {
            libelle: doc.libelle,
            publisher: doc.publisher,
            photo: doc.photo,
            file: doc.file,
            video: doc.video,
            content: doc.content ,
            level: doc.level,
            subject: doc.subject, 
            likes_count: doc.likes_count, 
            dislikes_count: doc.dislikes_count, 
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/extracurriculars/get" + doc._id
            }
          };
        })
      };
      //   if (docs.length >= 0) {
      res.status(200).json(response);
      //   } else {
      //       res.status(404).json({
      //           message: 'No entries found'
      //       });
      //   }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

//router.post("/post", upload.single('photo'), (req, res, next) => {
  exports.post = (req, res) => {
  const extracurricular = new Extracurricular({
    _id: new mongoose.Types.ObjectId(),
    libelle: req.body.libelle,
    publisher: req.body.publisher,
    photo: req.file.path,
    file: req.body.file,
    video: req.body.video,
    content: req.body.content ,
    level: req.body.level,
    subject: req.body.subject, 
    likes_count: req.body.likes_count,
    dislikes_count: req.body.dislikes_count
  });
  extracurricular
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created Extracurricular successfully",
        createdExtracurricular: {
            libelle: result.libelle,
            publisher: result.publisher,
            file: result.file,
            video: result.video,
            content: result.content ,
            level: result.level,
            subject: result.subject, 
            likes_count: result.likes_count,
            dislikes_count: result.dislikes_count,
            _id: result._id,
            request: {
                type: 'GET',
                url: "http://localhost:3000/extracurriculars/post" + result._id
            }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

//router.get("get/:extracurricularId", (req, res, next) => {
  exports.getone = (req, res) => {
  const extracurricularId = req.params.extracurricularId;
  Extracurricular.findById(extracurricularId)
    .select('libelle publisher photo  file  video content level subject likes_count dislikes_count ')
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
          extracurricular: doc,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/extracurriculars/get/:extracurricularId'
            }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

//router.patch("/:extracurricularId", upload.single('photo'),  (req, res, next) => {
exports.update = (req, res) => {
// Find Extracurricular and update it with the request body
Extracurricular.findByIdAndUpdate(req.params.extracurricularId, {
    libelle: req.body.libelle,
    publisher: req.body.publisher,
    photo: req.file.path,
    file: req.file.file,
    video: req.body.video,
    content: req.body.content ,
    level: req.body.level,
    subject: req.body.subject, 
    likes_count: req.body.likes_count,
    dislikes_count: req.body.dislikes_count
}, {new: true})
.then(extracurricular=> {
    if(!extracurricular) {
        return res.status(404).send({
            message: "Extracurricular not found with id " + req.params.extracurricularId
        });
    }
    res.send(extracurricular);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "extracurricular not found with id " + req.params.extracurricularId
        });                
    }
    return res.status(500).send({
        message: "Error updating extracurricular with id " + req.params.extracurricularId
    });
});
};


/*router.patch("/update/extracurriculars/like/:extracurricularId", (req, res, next) => {

  Extracurricular.findById(req.params.id)
  .exec()
    .then((like) => {
      like.count++;

      like.save()
        .then(() => res.json(like))
        .catch((err) => next(err));
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});*/


//router.delete("/:extracurricularId", (req, res, next) => {
exports.delete = (req, res) => {
  const id = req.params.extracurricularId;
  Extracurricular.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Extracurricular deleted',
          request: {
              type: 'DELETE',
              url: 'http://localhost:3000/extracurriculars/id',
              body: { libelle: 'String', publisher: 'String',  file: 'String',  video: 'String', content: 'String', level: 'Number', subject: 'String',likes_count:'Number', dislikes_count:'Number'}
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
/****************Crud to use "referance with user" ******************* */
exports.getAllExtracurricular= async (req, res, next) => {
  try {
    const extracurriculars = await Extracurricular.find({});
    res.status(200).json(extracurriculars);
  } catch (err) {
    next(err);
  }

};


exports.getUser= async (req, res, next) => {
  try {
    const users = await User.findById(req.body.libelle);
    console.log("",req.body);
    console.log(users);
    res.status(200).json(users);
  } catch (err) {
    next(err);
  }

};

exports.newExtracurricular = async (req, res, next) => {
  try {
    // find actuel user
    //const users = await User.findById('extracurricularId');
    const users = await User.findById('5f19ad196cb62e575058db22');
    console.log(users);
    //create a new extracurricular
    const newExtracurricular = {
      _id: new mongoose.Types.ObjectId(),
      libelle: req.body.libelle,
      publisher: req.body.publisher,
      photo: req.file.path,
      file: req.file.file,
      video: req.body.video,
      content: req.body.content ,
      level: req.body.level,
      subject: req.body.subject, 
      likes_count: req.body.likes_count,
      dislikes_count: req.body.dislikes_count
    };
    //fonctionnel sans delete 
    delete newExtracurricular.users;
    const extracurricular= new Extracurricular(newExtracurricular);
    extracurricular.users = users;
    await extracurricular.save();
    // add the newly created studentAbsence to the actuel student
    users.extracurriculars.push(extracurricular);
    await users.save();
    res.status(200).json(extracurricular);

  } catch (err) {
    next(err);
  }

};

exports.getoneExtracurricular = async (req, res, next) => {
  try {
   const extracurricular = await Extracurricular.findById(req.params.extracurricularId);
   res.status(200).json(extracurricular);
  } catch (err) {
    next(err);
  }

};

exports.replaceExtracurricular = async (req, res, next) => {
  try {
    const result = await Extracurricular.findByIdAndUpdate(req.params.extracurricularId, {
      libelle: req.body.libelle,
      publisher: req.body.publisher,
      photo: req.file.path,
      file: req.body.file,
      video: req.body.video,
      content: req.body.content ,
      level: req.body.level,
      subject: req.body.subject, 
      likes_count: req.body.likes_count,
      dislikes_count: req.body.dislikes_count
    });  
   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.updateExtracurricular = async (req, res, next) => {
  try {
   const result = await Extracurricular.findByIdAndUpdate(req.params.extracurricularId, {
    libelle: req.body.libelle,
    publisher: req.body.publisher,
    photo: req.file.path,
    file: req.body.file,
    video: req.body.video,
    content: req.body.content ,
    level: req.body.level,
    subject: req.body.subject, 
    likes_count: req.body.likes_count,
    dislikes_count: req.body.dislikes_count
  });

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.deleteExtracurricular = async (req, res, next) => {
  try {
    const extracurricularId = req.params.extracurricularId;
   //get extracurricular
   const extracurricular= await Extracurricular.findById(extracurricularId);
   if (!extracurricular){
     return res.status(404).json({error : 'we can not found this extracurricular'});
   }
   console.log(extracurricular);
   //const extracurricular = await Extracurricular.findById(req.params.extracurricularId);
   const userId = extracurricular.users;
   console.log(userId);
   // get user
   const users = await User.findById(userId);
   console.log(users);
   //remove extracurricular
   await extracurricular.remove();
   console.log(extracurricular);
   console.log(users);
   //remove from userlist
   users.extracurriculars.pull(extracurricular);
   await users.save();

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};






//module.exports = router;
/**************get & post  Comments from Extracurriculars ************* */
exports.getExtracurricularComment= async(req, res, next) => {
  try {
  const {extracurricularId} = req.params;
  const extracurricular= await Extracurricular.findById(extracurricularId).sort([['updatedAt', 'descending']]).populate('comments');
  res.status(200).json(extracurricular.comments)
  } catch (err) {
  next(err);
}
};

exports.newExtracurricularComment= async(req, res, next) => {
  try {
  const {extracurricularId} = req.params;
  const newComment = new Comment({
    content: req.body.content,   
  });
  //get extracurricular
  const extracurricular = await Extracurricular.findById(extracurricularId);
  //assign comment to extracurricular
  newComment.comments = extracurricular;
  //save the extracurricular
   await newComment.save();
  // add comment to extracurricular
  extracurricular.comments.push(newComment);
  //save the extracurricular
   await extracurricular.save();

  res.status(200).json(newComment);
} catch (err) {
  next(err);
}
};

exports.updateLike = async (req, res, next) => {
  try {
   const result = await Extracurricular.findByIdAndUpdate(req.params.extracurricularId, {
    $inc: { likes_count:1 }
  });

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};
exports.updateDislike = async (req, res, next) => {
  try {
   const result = await Extracurricular.findByIdAndUpdate(req.params.extracurricularId, {
    $inc: { dislikes_count:1 }
  });

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};


exports.statistique = async (req, res, next) => {
  try {
   const result = await Extracurricular.find({}).count();
   res.status(200).json(result);
  } catch (err) {
    next(err);
  }

};


