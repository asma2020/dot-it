const Extracurricular = require('../models/extracurricular.model.js');


exports.update = (req, res) => {
    // Validate Request
    const dislikes_count= req.body.dislikes_count;
    const id = req.params.extracurricularId;
    Extracurricular.findOne({extracurricularId: id, dislikes_count: req.body.dislikes_count}, function(err, extracurricular){
      if(err)return handleErr(err);
      extracurricular.dislikes_count = dislikes_count++;
       extracurricular.save(function(err){
        if(err)return handleErr(err);
      });
    });
}

