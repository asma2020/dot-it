const Extracurriculars = require('../../apiExtracurricular/models/extracurricular.model.js');
const mongoose = require('mongoose');Schema = mongoose.Schema;

const CommentsSchema = mongoose.Schema({
    content: { type: String, required: true },
    extracurriculars:[{
        type:Schema.Types.ObjectId,
        ref:"Extracurriculars"
    }],
}, {
    timestamps: true
});

module.exports = mongoose.model('Comments', CommentsSchema);

