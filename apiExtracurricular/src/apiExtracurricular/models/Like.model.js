const Extracurriculars = require('../../apiExtracurricular/models/extracurricular.model.js');
const mongoose = require('mongoose');Schema = mongoose.Schema;

const LikeSchema = mongoose.Schema({
    likes_count: { type: Number, default: 0 }, 
    extracurriculars:[{
        type:Schema.Types.ObjectId,
        ref:"Extracurriculars"
    }],
}, {
    timestamps: true
});

module.exports = mongoose.model('Like', LikeSchema);
