const Comments = require('../models/comment.model.js');
const User = require('../models/User.js');
const mongoose = require('mongoose');Schema = mongoose.Schema;

const ExtracurricularsSchema = mongoose.Schema({
    libelle: { type: String, required: true },
    publisher: { type: String, required: true },
    photo: { type: String, required: false },
    file: { type: String, required: false },
    video: { type: String, required: false },
    content: { type: String, required: true },
    level: { type: Number, required: true },
    subject: { type: String, required: true }, 
    likes_count: { type: Number, default: 0 }, 
    dislikes_count: { type: Number, default: 0 }, 
    users:[{
        type:Schema.Types.ObjectId,
        ref:"User"
    }],
    comments:[{
        type:Schema.Types.ObjectId,
        ref:"Comments"
    }],
}, {
    timestamps: true
});

module.exports = mongoose.model('Extracurriculars', ExtracurricularsSchema);