require("dotenv").config();
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios');
const mongoose = require("mongoose");
mongoose.connect(process.env.DATABASE, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
});

mongoose.set('useCreateIndex', true);

mongoose.connection.on("error", (err) => {
  console.log("Mongoose Connection ERROR: " + err.message);
});

mongoose.connection.once("open", () => {
  console.log("MongoDB Connected!");
});


const User = require('./models/User');
const app = express();
app.use(bodyParser.json());
app.use(cors());

const handleEvent = async  (type, data) => {


  if (type === 'Authentification') {
    const { email, password } = data;
    //users.push({ email, password });
    console.log(data);
  }
  if (type === 'Registration') {

    const {
      _id,
      name,
      email,
      password,
      role,
      registrationNumber,
      birthdate,
      phoneNumber,
      address,
      salary,
      gender,
      category,
      contractDueDate,
      contractType,
      nic,
      status,
      photo,
      school_level,
      last_school,
      study_fees,
      schoolYears,
      quarters,
      classes,
      schedules,
      sessions,
      subjects,
      tests } = data;
    const user =await User.create(data)
    
    console.log("user after save",user)



    //  console.log("user",user)
  }
  /*if (type === 'CommentCreated') {
    const { id, content, postId, status } = data;

    //Getting the specific post for which this comment has been made
    const post = posts[postId];
    //Adding this comment to the comments array of the appropriate post
    post.comments.push({ id, content, status });*/
  //comment.status = status;
  //comment.content = content;

};

app.post('/events', (req, res) => {
  const { type, data } = req.body;

  handleEvent(type, data);


  res.send({});
});



app.listen(4002, async () => {
  console.log('Listening on 4002');

  const res = await axios.get('http://localhost:4005/events');

  for (let event of res.data) {
    console.log('Processing event: ', event.type);
    handleEvent(event.type, event.data);
  }
});
