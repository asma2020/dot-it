const express = require("express");

const app = express();
//const mongodb = require('mongodb');
const bodyParser = require('body-parser');
const morgan = require("morgan");
//const extracurricularRoutes = require("./apiExtracurricular/controllers/extracurricular");

//app.use(express.json());
//app.use(express.urlencoded({ extended: true }));

//Setup Cross Origin
app.use(require("cors")());
//Upload Image
app.use(bodyParser.json())
app.use(morgan("dev"));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Bring in the apiSchool routes
//app.use("/extracurriculars",require("./apiExtracurricular/routes/extracurricular.routes.js"));
//Require Chat routes
//app.use("/user", require("./routes/user"));
//app.use("/chatroom", require("./routes/chatroom"));
//Bring in the apiAbsence routes
app.use("/absences",require("./apiAbsence/routes/Absence.routes.js"));
//Setup Error Handlers


//Creating the event request handler (to handle the events published by the event-bus)
/*app.post('/events', (req, res) => {
  console.log('Received login Event', req.body.type);
  res.send({});
});*/

module.exports = app;
