const express = require('express');
const mongoose = require("mongoose");
const Absence = require("../models/Absences.model");
const User = require("../models/User"); 
/************crud simple juste pour tester le front*************/
  exports.getall = (req, res) => {
    Absence.find()
    .select("designation hours_number justification")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        absences: docs.map(doc => {
          return {
            designation: doc.designation,
            hours_number: doc.hours_number,
            justification: doc.justification,
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/absences/get" + doc._id
            }
          };
        })
      };

      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.post = (req, res) => {
  const absence = new Absence({
    _id: new mongoose.Types.ObjectId(),
    designation: req.body.designation,
            designation: req.body.designation,
            hours_number: req.body.hours_number,
            justification: req.body.justification,
  });
  absence
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created Absence successfully",
        createdAbsence: {
            designation: result.designation,
            hours_number: result.hours_number,
            justification:result.justification,
            _id: result._id,
            request: {
                type: 'GET',
                url: "http://localhost:3000/absences/post" + result._id
            }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.getone = (req, res) => {
  const id = req.params.absenceId;
  Absence.findById(id)
    .select('designation  hours_number justification')
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
            absence: doc,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/absences/get/:id'
            }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.update = (req, res) => {
// Find Absence and update it with the request body
Absence.findByIdAndUpdate(req.params.absenceId, {
    designation: req.body.designation,
    hours_number: req.body.hours_number,
    justification:req.body.justification,
}, {new: true})
.then(absence=> {
    if(!absence) {
        return res.status(404).send({
            message: "Absence not found with id " + req.params.quarterId
        });
    }
    res.send(absence);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "Absence not found with id " + req.params.absenceId
        });                
    }
    return res.status(500).send({
        message: "Error updating absence with id " + req.params.absenceId
    });
});
};

exports.delete = (req, res) => {
  const id = req.params.absenceId;
  Absence.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Absence deleted',
          request: {
              type: 'DELETE',
              url: 'http://localhost:3000/absences/id',
              body: { designation: 'String',  hours_number: 'Number',  justification: 'Boolean' }
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
/*******************le code a utiliser **********************/
exports.getAllAbsence= async (req, res, next) => {
  try {
    const absences = await Absence.find({});
    res.status(200).json(absences);
  } catch (err) {
    next(err);
  }

};

/*exports.newAbsence = async (req, res, next) => {
  try {
    // find actuel user
    const  users = await User.findById(req.body.users);
    //create a new Absence
    
    const newAbsence = {
      _id: new mongoose.Types.ObjectId(),
      hours_number: req.body.hours_number,
      justification:req.body.justification,
    };
    delete newAbsence.users;
    const absence = new Absence(newAbsence);
    absence.users = users;
    await absence.save();
    // add the newly created Absence to the actuel user
    users.absences.push(absence);
    await users.save();
    res.status(200).json(absence);

  } catch (err) {
    next(err);
  }

};*/

exports.newAbsence = async (req, res, next) => {
  try {
    // find actuel student
    const  users = await User.findById(req.body.user);
    //create a new StudentAbsence
    const newUserAbsence = {
      _id: new mongoose.Types.ObjectId(),
      designation: req.body.designation,
      hours_number: req.body.hours_number,
      justification:req.body.justification,
    };

    delete newUserAbsence.users;
    const userAbsence = new Absence(newUserAbsence);
    userAbsence.users = users;
    await userAbsence.save();
    // add the newly created studentAbsence to the actuel student
    users.absences.push(userAbsence);
    await users.save();
    res.status(200).json(userAbsence);

  } catch (err) {
    next(err);
  }

};



exports.getoneUserAbsence = async (req, res, next) => {
  try {
   const absence = await Absence.findById(req.params.absenceId);
   res.status(200).json(absence);
  } catch (err) {
    next(err);
  }

};

exports.replaceUserAbsence = async (req, res, next) => {
  try {
    const result = await Absence.findByIdAndUpdate(req.params.absenceId, {
      designation: req.body.designation,
      hours_number: req.body.hours_number,
      justification:req.body.justification,
    });  
   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

/*exports.updateUserAbsence = async (req, res, next) => {
  try {
   const result = await Absence.findByIdAndUpdate(req.params.absenceId, { body: req.body.body }, (err, post) => {
    let Pusher = require('pusher');
    let pusher = new Pusher({
        appId: process.env.PUSHER_APP_ID,
        key: process.env.PUSHER_APP_KEY,
        secret: process.env.PUSHER_APP_SECRET,
        cluster: process.env.PUSHER_APP_CLUSTER
    });

    pusher.trigger('notifications', 'absence-updated', post, req.headers['x-socket-id']);
    res.send('');
});
   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};*/


exports.deleteUserAbsence = async (req, res, next) => {
  try {
  const absenceId = req.params.absenceId;
  console.log(absenceId);
   //get user Absence
   const absence = await Absence.findById(req.params.absenceId);
   //const absence= await Absence.findById(absenceId);
   console.log(absence);
   if (!absence){
     return res.status(404).json({error : 'we can not found this absence'});
   }
   console.log(absence);
   const userId = absence.users;
   console.log(userId);
   // get user
   const users = await User.findById(userId);
   console.log(users);
   //remove Absence 
   await absence.remove();
   console.log(absence);
   console.log(users);
   //remove from user list
   users.absences.pull(absence);
   await users.save();

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};
/******************** nbr d'absence*************/
exports.countUsersAbsences= async (req, res, next) => {
  try {
    const absences = await Absence.find({}).count();
    res.status(200).json(absences);
  } catch (err) {
    next(err);
  }

};