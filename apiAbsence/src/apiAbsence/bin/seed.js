require("dotenv").config();
const mongoose = require("mongoose");
mongoose.connect(process.env.DATABASE, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
});

const faker = require('faker');
const Post = require('../models/Absences.model.js');

// empty the collection first
Post.remove({})
    .then(() => {
        const absences = [];
        for (let i = 0; i < 5; i++) {
            absences.push({
            designation: faker.lorem.sentence(),
            hours_number: faker.lorem.number(),
            justification: faker.lorem.paragraph()
        });
    }
        return Absence.create(absences);
    })
    .then(() => {
        process.exit();
    })
    .catch((e) => {
        console.log(e);
        process.exit(1);
    });
