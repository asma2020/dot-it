const User = require('../models/User.js');
const mongoose = require('mongoose');Schema = mongoose.Schema;

const AbsencesSchema = mongoose.Schema({
    designation: { type: String, required: true },
    hours_number: { type: Number, required: true },
    justification: { type: Boolean, required: true , default: false },
   users:[{
        type:Schema.Types.ObjectId,
        ref:"User"
    }],
}, {
    timestamps: true
});

module.exports = mongoose.model('Absences', AbsencesSchema);


