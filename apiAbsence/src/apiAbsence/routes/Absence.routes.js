const router = require("express").Router();
const mongoose = require("mongoose");
const AbsenceController = require("../controllers/Absence.controller.js");

/************crud simple juste pour tester le front*************/
router.get("/get", AbsenceController.getall);
router.post("/post", AbsenceController.post);
router.get("get/:absenceId",AbsenceController.getone);
router.patch("/:absenceId", AbsenceController.update);
router.delete("/:absenceId", AbsenceController.delete);
/************crud a utiliser*************/
router.get("/getAllAbsence", AbsenceController.getAllAbsence);
router.post("/newAbsence", AbsenceController.newAbsence );
router.get("/getoneUserAbsence/:absenceId", AbsenceController.getoneUserAbsence);
router.patch("/replaceUserAbsence/:absenceId", AbsenceController.replaceUserAbsence);
//router.put("/updateUserAbsence/:absenceId", AbsenceController.updateUserAbsence );
router.delete("/deleteUserAbsence/:absenceId", AbsenceController.deleteUserAbsence );
/*********count Student Absences******** */
router.get("/countUsersAbsences", AbsenceController.countUsersAbsences);

module.exports = router;
