const router = require("express").Router();
const mongoose = require("mongoose");
const multer = require('multer');
const sessionController = require("../controllers/Session.controller.js");


const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
      //cb(null, new Date().toISOString() + file.originalname);
      cb(null, Date.now() + file.originalname); 
    }
  });
  
  const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
  
  const upload = multer({
      storage: storage,
      limits: {
       fileSize: 1024 * 1024 * 5
      },
      fileFilter: fileFilter
    });
  








/************crud simple juste pour tester le front*************/
router.get("/get", sessionController.getall);
router.post("/post", sessionController.post);
router.get("get/:sessionId",sessionController.getone);
router.patch("/:sessionId", sessionController.update);
router.delete("/:sessionId", sessionController.delete);
/************crud reel a utiliser*************/
router.get("/getAllSchedule", sessionController.getAllSchedule);
router.post("/newSession", sessionController.newSession);
router.get("/getonesessionschedule/:sessionId", sessionController.getonesessionschedule);
router.patch("/replacesessionschedule/:sessionId", sessionController.replacesessionschedule);
router.put("/updatesessionschedule/:sessionId", sessionController.updatesessionschedule );
router.delete("/deletesessionschedule/:sessionId", sessionController.deletesessionschedule );
/************* */
//get all Subject from Schedule
router.get("/:sessionId/getSubject", sessionController.getSessionSubject);
//post a new Subject to a Schedule 
router.post("/:sessionId/postSubject",upload.single('photo'), sessionController.newSessionSubject);

module.exports = router;