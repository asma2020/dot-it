const router = require("express").Router();
const mongoose = require("mongoose");
const statistiqueController = require("../controllers/Statistique.controller");

// get statistique 
router.get("/statistiqueSchoolYear", statistiqueController.statistiqueSchoolYear );
router.get("/statistiqueQuarter", statistiqueController.statistiqueQuarter);
router.get("/statistiquesClasse ", statistiqueController.statistiquesClasse);
router.get("/statistiqueSchedule", statistiqueController.statistiqueSchedule);
router.get("/statistiqueSession", statistiqueController.statistiqueSession);
router.get("/statistiqueSubject", statistiqueController.statistiqueSubject);
router.get("/statistiqueTest", statistiqueController.statistiqueTest);
router.get("/statistiquesStudent ", statistiqueController.statistiquesStudent);

module.exports = router;
