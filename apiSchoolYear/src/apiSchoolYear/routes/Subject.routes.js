const router = require("express").Router();
const mongoose = require("mongoose");
const multer = require('multer');
const subjectController = require("../controllers/Subject.controller.js");


const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
      //cb(null, new Date().toISOString() + file.originalname);
      cb(null, Date.now() + file.originalname); 
    }
  });
  
  const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
  
  const upload = multer({
      storage: storage,
      limits: {
       fileSize: 1024 * 1024 * 5
      },
      fileFilter: fileFilter
    });
/************crud simple juste pour tester le front*************/
router.get("/get", subjectController.getall);
router.post("/post"/*,upload.single('photo')*/, subjectController.post);
router.get("get/:subjectId", subjectController.getone);
router.patch("/:subjectId"/*,upload.single('photo')*/, subjectController.update);
router.delete("/:subjectId", subjectController.delete);
/************crud reel a utiliser*************/
router.get("/getAllSubject", subjectController.getAllSession);
router.post("/newSubject",/*upload.single('photo'),*/subjectController.newSubject);
router.get("/getSubjectSession/:subjectId", subjectController.getonesubjectsession);
router.patch("/replaceSubjectSession/:subjectId",/*upload.single('photo'), */subjectController.replacesubjectsession);
router.put("/updateSubjectSession/:subjectId",/*upload.single('photo'),*/ subjectController.updatesubjectsession );
router.delete("/deleteSubjectSession/:subjectId", subjectController.deletesubjectsession);
/************* */
//get all subject tests 
router.get("/:subjectId/getSubject", subjectController.getSubjectTest);
//post test from subject
router.post("/:subjectId/postSubject", subjectController.newSubjectTest);



module.exports = router;
