const router = require("express").Router();
const mongoose = require("mongoose");
const multer = require('multer');
const eventController = require("../controllers/Event.controller.js");
const schoolYear = require("../controllers/SchoolYear.controller.js");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    //cb(null, new Date().toISOString() + file.originalname);
    cb(null, Date.now() + file.originalname); 
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
    storage: storage,
    limits: {
     fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
  });
/************crud simple juste pour tester le front*************/
router.get("/get", eventController.getall);
router.post("/post",upload.single('photo'), eventController.post);
router.get("get/:eventId",eventController.getone);
router.patch("/:eventId",upload.single('photo'), eventController.update);
router.delete("/:eventId", eventController.delete);
/************crud a utiliser*************/
router.get("/getAllSchool", eventController.getAllSchool);
router.post("/newEvent",upload.single('photo'), eventController.newEvent);
router.get("/getEventschool/:eventId", eventController.getoneeventschool);
router.patch("/replaceEventschool/:eventId",upload.single('photo'), eventController.replaceeventschool);
router.put("/updateEventschool/:eventId",upload.single('photo'), eventController.updateeventschool );
router.delete("/deleteEventschool/:eventId", eventController.deleteeventschool );



module.exports = router;
