const router = require("express").Router();
const mongoose = require("mongoose");
const multer = require('multer');
const vacationController = require("../controllers/Vacations.controller.js");
const schoolYear = require("../controllers/SchoolYear.controller.js");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    //cb(null, new Date().toISOString() + file.originalname);
    cb(null, Date.now() + file.originalname); 
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
    storage: storage,
    limits: {
     fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
  });
/************crud simple juste pour tester le front*************/
router.get("/get", vacationController.getall);
router.post("/post",upload.single('photo'), vacationController.post);
router.get("get/:vacationId",vacationController.getone);
router.patch("/:vacationId",upload.single('photo'), vacationController.update);
router.delete("/:vacationId", vacationController.delete);
/************crud reel a utiliser*************/
router.get("/getAllSchool", vacationController.getAllSchool);
router.post("/newVacation",upload.single('photo'), vacationController.newVacation);
router.get("/getVacationschool/:vacationId", vacationController.getonevacationschool);
router.patch("/replaceVacationschool/:vacationId",upload.single('photo'), vacationController.replacevacationschool);
router.put("/updateVacationschool/:vacationId",upload.single('photo'), vacationController.updatevacationschool );
router.delete("/deleteVacationschool/:vacationId", vacationController.deletevacationschool );



module.exports = router;
