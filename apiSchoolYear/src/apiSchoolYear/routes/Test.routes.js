const router = require("express").Router();
const mongoose = require("mongoose");
const multer = require('multer');
const testController = require("../controllers/Test.controller.js");
const Subject = require("../models/Subjects.model"); 




const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
      cb(null, new Date().toISOString() + file.originalname);
    }
  });
  
  const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
  
  const upload = multer({
      storage: storage,
      limits: {
       fileSize: 1024 * 1024 * 5
      },
      fileFilter: fileFilter
    });
  
/************crud simple juste pour tester le front*************/
router.get("/get", testController .getall);
router.post("/post", testController .post);
router.get("get/:testId",testController .getone);
router.patch("/:testId", testController .update);
router.delete("/:testId", testController .delete);
/************crud reel a utiliser*************/
router.get("/getAllTest", testController.getAllTest);
router.post("/newTest", testController.newTest);
router.get("/getTestSubject/:testId", testController.getonetestsubject);
router.patch("/replaceTestSubject/:testId", testController.replacetestsubject);
router.put("/updateTestSubject/:testId", testController.updatetestsubject );
router.delete("/deleteTestSubject/:testId",testController.deletetestsubject  );


module.exports = router;
