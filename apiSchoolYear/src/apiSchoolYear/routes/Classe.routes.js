module.exports = (app) => {
    const classes = require('../controllers/classe.controller.js');

    // Create a new Classe
    app.post('/classes', classes.create);

    // Retrieve all Classes
    app.get('/classes', classes.findAll);

    // Retrieve a single Classe with classeId
    app.get('/classes/:classeId', classes.findOne);

    // Update a Classe with classeId
    app.put('/classes/:classeId', classes.update);

    // Delete a Classe with classeId
    app.delete('/classes/:classeId', classes.delete);
}



const router = require("express").Router();
const mongoose = require("mongoose");
const multer = require('multer');
const classeController = require('../controllers/classe.controller.js');
const schoolYear = require("../controllers/SchoolYear.controller.js");





const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
      cb(null, new Date().toISOString() + file.originalname);
    }
  });
  
  const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
  
  const upload = multer({
      storage: storage,
      limits: {
       fileSize: 1024 * 1024 * 5
      },
      fileFilter: fileFilter
    });
  


/************crud simple juste pour tester le front*************/
router.get("/get", classeController.getall);
router.post("/post", classeController.post);
router.get("get/:classeId",classeController.getone);
router.patch("/:classeId", classeController.update);
router.delete("/:classeId", classeController.delete);
/************crud a utiliser*************/
router.get("/getAllQuarter", classeController.getAllQuarter);
router.post("/newClasse", classeController.newClasse);
router.get("/getClasseQuarter/:classeId", classeController.getoneclassequarter);
router.patch("/replaceClasseQuarter/:classeId", classeController.replaceclassequarter);
router.put("/updateClasseQuarter/:classeId", classeController.updateclassequarter );
router.delete("/deleteClasseQuarter/:classeId", classeController.deleteclassequarter );
/************* */
//get all quarter classes
router.get("/:classeId/getSchedule",upload.single('photo'), classeController.getClasseSchedule);
//post a new classe to a quarter   
router.post("/:classeId/postSchedule",upload.single('photo'),classeController.newClasseSchedule);
/************* */
//get all quarter subjects 
//router.get("/:quarterId/getTest", quarterController.getQuarterSubject);
//get all quarter subjects  
//router.post("/:quarterId/postTest",upload.single('photo'), quarterController.newQuarterSubject);



module.exports = router;