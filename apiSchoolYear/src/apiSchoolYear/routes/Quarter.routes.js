const router = require("express").Router();
const mongoose = require("mongoose");
const multer = require('multer');
const quarterController = require("../controllers/Quarters.controller.js");
const schoolYear = require("../controllers/SchoolYear.controller.js");


const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
      //cb(null, new Date().toISOString() + file.originalname);
      cb(null, Date.now() + file.originalname); 
    }
  });
  
  const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
  
  const upload = multer({
      storage: storage,
      limits: {
       fileSize: 1024 * 1024 * 5
      },
      fileFilter: fileFilter
    });
  


/************crud simple juste pour tester le front*************/
router.get("/get", quarterController.getall);
router.post("/post", quarterController.post);
router.get("get/:quarterId",quarterController.getone);
router.patch("/:quarterId", quarterController.update);
router.delete("/:quarterId", quarterController.delete);
/************crud a utiliser*************/
router.get("/getAllSchool", quarterController.getAllSchool);
router.post("/newQuarter", quarterController.newQuarter);
router.get("/getQuarterschool/:quarterId", quarterController.getonequarterschool);
router.patch("/replaceQuarterschool/:quarterId", quarterController.replacequarterschool);
router.put("/updateQuarterschool/:quarterId", quarterController.updatequarterschool );
router.delete("/deleteQuarterschool/:quarterId", quarterController.deletequarterschool );
/************* */
//get all quarter classes
router.get("/:quarterId/getClasse", quarterController.getQuarterClasse);
//post a new classe to a quarter   
router.post("/:quarterId/postClasse", quarterController.newQuarterClasse);
/************* */
//get all quarter subjects 
//router.get("/:quarterId/getTest", quarterController.getQuarterSubject);
//get all quarter subjects  
//router.post("/:quarterId/postTest",upload.single('photo'), quarterController.newQuarterSubject);



module.exports = router;
