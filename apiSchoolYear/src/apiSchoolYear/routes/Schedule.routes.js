const router = require("express").Router();
const mongoose = require("mongoose");
const multer = require('multer');
const scheduleController = require("../controllers/Schedule.controller.js");

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
      //cb(null, new Date().toISOString() + file.originalname);
      cb(null, Date.now() + file.originalname); 
    }
  });
  
  const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
  
  const upload = multer({
      storage: storage,
      limits: {
       fileSize: 1024 * 1024 * 5
      },
      fileFilter: fileFilter
    });
/************crud simple juste pour tester le front*************/
router.get("/get", scheduleController.getall);
router.post("/post",upload.single('photo'), scheduleController.post);
router.get("get/:scheduleId",scheduleController.getone);
router.patch("/:scheduleId",upload.single('photo'),scheduleController.update);
router.delete("/:scheduleId",scheduleController.delete);
/************crud reel a utiliser*************/
router.get("/getAllClasse", scheduleController.getAllClasse);
router.post("/newSchedule",upload.single('photo'), scheduleController.newSchedule);
router.get("/getonescheduleclasse/:scheduleId", scheduleController.getonescheduleclasse);
router.patch("/replacescheduleclasse/:scheduleId",upload.single('photo'), scheduleController.replacescheduleclasse);
router.put("/updatescheduleclasse/:scheduleId", upload.single('photo'),scheduleController.updatescheduleclasse );
router.delete("/deletescheduleclasse/:scheduleId",upload.single('photo'),scheduleController.deletescheduleclasse  );
/************* */
//get all schedule classes
router.get("/:scheduleId/getSchedule",scheduleController.getSessionSchedule);
//post a new classe to a schedule  
router.post("/:scheduleId/postSchedule",scheduleController.newSessionSchedule);


module.exports = router;
