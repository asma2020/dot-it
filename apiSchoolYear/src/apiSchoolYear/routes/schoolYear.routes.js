const router = require("express").Router();
const mongoose = require("mongoose");
const multer = require('multer');
const schoolYearController = require("../controllers/SchoolYear.controller.js");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    //cb(null, new Date().toISOString() + file.originalname);
    cb(null, Date.now() + file.originalname);  
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
    storage: storage,
    limits: {
     fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
  });

router.get("/get", schoolYearController.getall);
router.post("/post",upload.single('photo'), schoolYearController.post);
router.get("get/:schoolYearId",schoolYearController.getone);
router.patch("/:schoolYearId",upload.single('photo'), schoolYearController.update);
router.delete("/:schoolYearId", schoolYearController.delete);
//get all school years events 
router.get("/:schoolYearId/getEvent", schoolYearController.getSchoolYearEvent);
//get all school years events 
router.post("/:schoolYearId/postEvent",upload.single('photo'), schoolYearController.newSchoolYearEvent);
/*******************************************/
//get all school years Vacances 
router.get("/:schoolYearId/getVacation", schoolYearController.getSchoolYearVacance);
//add a school years vacance 
router.post("/:schoolYearId/postVacation",upload.single('photo'), schoolYearController.newSchoolYearVacation);
/*******************************************/
//get all school years Quarters
router.get("/:schoolYearId/getQuarter", schoolYearController.getSchoolYearQuarter);
//add a school years Quarter
router.post("/:schoolYearId/postQuarter",upload.single('photo'), schoolYearController.newSchoolYearQuarter);
/*********************************** */
//get all school years subjets
router.get(":subjectId/getQuarter", schoolYearController.getSubjectSchoolYear);
//get all school years tests
router.post("/:subjectId/getSubject", schoolYearController.getTestSchoolYear);
/*********************************** */
//get users
router.get("/:schoolYearId/getUser", schoolYearController.getSchoolYearUser);
//post user
router.post("/:schoolYearId/postUser", schoolYearController.newSchoolYearUser);

module.exports = router;
