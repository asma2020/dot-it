const express = require('express');
const mongoose = require("mongoose");
const Quarter = require("../models/Quarters.model");
const SchoolYear = require("../models/SchoolYears.model"); 
const Subject = require("../models/Subjects.model");
const Classe = require("../models/Classes.model");

/************crud simple juste pour tester le front*************/
  exports.getall = (req, res) => {
    Quarter.find()
    .select("designation start_date end_date description student_quarterly_average")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        quarters: docs.map(doc => {
          return {
            designation: doc.designation,
            start_date: doc.start_date,
            end_date: doc.end_date,
            description: doc.description,
            student_quarterly_average: doc.student_quarterly_average, 
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/quarters/get" + doc._id
            }
          };
        })
      };

      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.post = (req, res) => {
  const quarter = new Quarter({
    _id: new mongoose.Types.ObjectId(),
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    description:req.body.description,
    student_quarterly_average:req.body.student_quarterly_average, 
  });
  quarter
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created Quarter successfully",
        createdQuarter: {
            designation: result.designation,
            start_date: result.start_date,
            end_date: result.end_date,
            description:result.description,
            _id: result._id,
            request: {
                type: 'GET',
                url: "http://localhost:3000/quarters/post" + result._id
            }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.getone = (req, res) => {
  const id = req.params.quarterId;
  Quarter.findById(id)
    .select('designation start_date end_date description student_quarterly_average')
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
            quarter: doc,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/quarters/get/:id'
            }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.update = (req, res) => {
// Find Quarter and update it with the request body
Quarter.findByIdAndUpdate(req.params.quarterId, {
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    description:req.body.description,
    student_quarterly_average: req.body.student_quarterly_average,
}, {new: true})
.then(quarter=> {
    if(!quarter) {
        return res.status(404).send({
            message: "Quarter not found with id " + req.params.quarterId
        });
    }
    res.send(quarter);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "Quarter not found with id " + req.params.quarterId
        });                
    }
    return res.status(500).send({
        message: "Error updating Quarter with id " + req.params.quarterId
    });
});
};

exports.delete = (req, res) => {
  const id = req.params.quarterId;
  Quarter.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Quarter deleted',
          request: {
              type: 'DELETE',
              url: 'http://localhost:3000/quarters/id',
              body: { designation: 'String',  start_date: 'Date', end_date:'Date',  description: 'String',   student_quarterly_average: 'Number'}
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
/*********************le code a utiliser ********************/
exports.getAllSchool= async (req, res, next) => {
  try {
    const quarters = await Quarter.find({});
    res.status(200).json(quarters);
  } catch (err) {
    next(err);
  }

};

 

exports.newQuarter = async (req, res, next) => {
  try {
    // find actuel school year
    const schoolYears = await SchoolYear.findById(req.body.schoolYears);
    //create a new Quarter
    const newQuarter = {
      _id: new mongoose.Types.ObjectId(),
      designation: req.body.designation,
      start_date: req.body.start_date,
      end_date: req.body.end_date,
      description:req.body.description,
      quarterly_average: req.body.student_quarterly_average,
    };

    delete newQuarter.schoolYears;
    const quarter = new Quarter(newQuarter);
    quarter.schoolYears = schoolYears;
    await quarter.save();
    // add the newly created quarter to the actuel school year
    schoolYears.quarters.push(quarter);
    await schoolYears.save();
    res.status(200).json(quarter);

  } catch (err) {
    next(err);
  }

};

exports.getonequarterschool = async (req, res, next) => {
  try {
   const quarter = await Quarter.findById(req.params.quarterId);
   res.status(200).json(quarter);
  } catch (err) {
    next(err);
  }

};

exports.replacequarterschool = async (req, res, next) => {
  try {
    const result = await Quarter.findByIdAndUpdate(req.params.quarterId, {
      designation: req.body.designation,
      start_date: req.body.start_date,
      end_date: req.body.end_date,
      description:req.body.description,
      student_quarterly_average: req.body.student_quarterly_average,
    });  
   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.updatequarterschool = async (req, res, next) => {
  try {
   const result = await Quarter.findByIdAndUpdate(req.params.quarterId, {
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    description:req.body.description,
    student_quarterly_average: req.body.student_quarterly_average,
  });

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.deletequarterschool = async (req, res, next) => {
  try {
    const quarterId = req.params.quarterId;
   //get quarter
   const quarter= await Quarter.findById(quarterId);
   if (!quarter){
     return res.status(404).json({error : 'we can not found this quarter'});
   }
   console.log(quarter);
   //const quarter = await Quarter.findById(req.params.quarterId);
   const schoolYearId = quarter.schoolYears;
   console.log(schoolYearId);
   // get school Year
   const schoolYears = await SchoolYear.findById(schoolYearId);
   console.log(schoolYears);
   //remove quarter 
   await quarter.remove();
   console.log(quarter);
   console.log(schoolYears);
   //remove from schoolYear list
   schoolYears.quarters.pull(quarter);
   await schoolYears.save();

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

/**************get & post classes from Quarters ************* */
exports.getQuarterClasse= async(req, res, next) => {
  try {
  const {quarterId} = req.params;
  const quarter= await Quarter.findById(quarterId).populate('classes');
  res.status(200).json(quarter.classes)
  } catch (err) {
  next(err);
}
};


exports.newQuarterClasse= async(req, res, next) => {
  try {
  const {quarterId} = req.params;
  const newClasse = new Classe({
    designation: req.body.designation,
    student_numbers: req.body.student_numbers,
    division: req.body.division,  
  });
  //get quarter
  const quarter = await Classe.findById(quarterId);
  //assign Classe to quarter
  newClasse.classes = quarter;
  //save the event
   await newClasse.save();
  // add Classe to quarters
  quarter.classes.push(newClasse);
  //save the quarter
   await quarter.save();

  res.status(200).json(newClasse);
} catch (err) {
  next(err);
}
};


/**************get & post Subjects from quarter************* */
/*exports.getQuarterSubject= async(req, res, next) => {
    try {
    const {quarterId} = req.params;
    const quarter= await Quarter.findById(quarterId).populate('subjects');
    res.status(200).json(quarter.subjects)
    } catch (err) {
    next(err);
  }
  };
  
  
  exports.newQuarterSubject= async(req, res, next) => {
    try {
    const {quarterId} = req.params;
    //const id = req.params.quarterId;
    const newSubject = new Subject({
      _id: new mongoose.Types.ObjectId(),
      designation: req.body.designation,
      coefficient: req.body.coefficient,
      photo: req.file.path,
      subject_student_average:req.body.subject_student_average,      
    });
    //get quarter
    const quarter = await Quarter.findById(quarterId);
    //assign subject to quarter
    newSubject.subjects = quarter;
    //save the event
     await newSubject.save();
    // add subject to quarters
    quarter.subjects.push(newSubject);
    //save the quarter
     await quarter.save();
  
    res.status(200).json(newSubject);
  } catch (err) {
    next(err);
  }
  };*/
