const SchoolYear = require('../models/SchoolYears.model.js');
const Quarter = require('../models/Quarters.model.js');
const Classe = require('../models/Classes.model.js');
const Schedule = require('../models/Schedules.model.js');
const Session = require('../models/Sessions.model.js');
const Subject = require('../models/Subjects.model.js');
const Test = require('../models/Tests.model.js');
const Student = require("../models/Students.model");

exports.statistiqueSchoolYear = async (req, res, next) => {
    try {
     const result = await SchoolYear.find({}).count();
     res.status(200).json(result);
    } catch (err) {
      next(err);
    }
  
  };
  exports.statistiqueQuarter = async (req, res, next) => {
    try {
     const result = await Quarter.find({}).count();
     res.status(200).json(result);
    } catch (err) {
      next(err);
    }
  
  };
  exports.statistiquesClasse = async (req, res, next) => {
    try {
     const result = await Classe.find({}).count();
     res.status(200).json(result);
    } catch (err) {
      next(err);
    }
  
  };
  exports.statistiqueSchedule = async (req, res, next) => {
    try {
     const result = await Schedule.find({}).count();
     res.status(200).json(result);
    } catch (err) {
      next(err);
    }
  
  };
  exports.statistiqueSession = async (req, res, next) => {
    try {
     const result = await Session.find({}).count();
     res.status(200).json(result);
    } catch (err) {
      next(err);
    }
  
  };
  exports.statistiqueSubject = async (req, res, next) => {
    try {
     const result = await Subject.find({}).count();
     res.status(200).json(result);
    } catch (err) {
      next(err);
    }
  
  };
  exports.statistiqueTest = async (req, res, next) => {
    try {
     const result = await Test.find({}).count();
     res.status(200).json(result);
    } catch (err) {
      next(err);
    }
  
  };
  exports.statistiquesStudent = async (req, res, next) => {
    try {
     const result = await Student.find({}).count();
     res.status(200).json(result);
    } catch (err) {
      next(err);
    }
  
  };