const express = require('express');
const mongoose = require("mongoose");
const Test = require("../models/Tests.model"); 
const Subject = require("../models/Subjects.model");
//const Student = require("../models/Students.model");
/************crud simple juste pour tester le front*************/
  exports.getall = (req, res) => {
    Test.find()
    .select("name type test_note test_coefficient")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        tests: docs.map(doc => {
          return {
            name: doc.name,
            type: doc.type,
            test_note: doc.test_note,
            test_coefficient: doc.test_coefficient,  
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/subjects/get" + doc._id
            }
          };
        })
      };

      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.post = (req, res) => {
  const test = new Test({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    type: req.body.type,
    test_note: req.body.test_note,
    test_coefficient:req.body.test_coefficient,  
  });
  test
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created Test successfully",
        createdTest: {
            name: result. name,
            type: result.type,
            test_note:result.test_note,
            test_coefficient:result.test_coefficient,
            _id: result._id,
            request: {
                type: 'GET',
                url: "http://localhost:3000/tests/post" + result._id
            }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.getone = (req, res) => {
  const id = req.params.testId;
  Test.findById(id)
    .select('name type test_note test_coefficient')
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
            test: doc,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/subjects/get/:id'
            }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.update = (req, res) => {
// Find Test and update it with the request body
Test.findByIdAndUpdate(req.params.testId, {
    name: req.body.name,
    type: req.body.type,
    test_note: req.body.test_note,
    test_coefficient:req.body.test_coefficient,  
}, {new: true})
.then(test => {
    if(!test) {
        return res.status(404).send({
            message: "Test not found with id " + req.params.testId
        });
    }
    res.send(test);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "Test not found with id " + req.params.testId
        });                
    }
    return res.status(500).send({
        message: "Error updating Test with id " + req.params.testId
    });
});
};

exports.delete = (req, res) => {
  const id = req.params.testId;
  Test.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Test deleted',
          request: {
              type: 'DELETE',
              url: 'http://localhost:3000/tests/id',
              body: { name: 'String',  type: 'String', test_note: 'Number', test_coefficient: 'Number'}
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

/******************le code a utiliser ***********************/
exports.getAllTest= async (req, res, next) => {
  try {
    const tests = await Test.find({});
    res.status(200).json(tests);
  } catch (err) {
    next(err);
  }

};

exports.newTest = async (req, res, next) => {
  try {
    //const students = await SchoolYear.findById(req.body.students);
    // find actuel Subject
    const subjects = await Subject.findById(req.body.subjects);
    //create a new Test
    const newTest = {
      _id: new mongoose.Types.ObjectId(),
      name: req.body.name,
      type: req.body.type,
      test_note: req.body.test_note,
      test_coefficient:req.body.test_coefficient,  
    };

    delete newTest.subjects;
    const test = new Test(newTest);
    test.subjects = subjects;
    await test.save();
    // add the newly created test to the actuel subject
    subjects.tests.push(test);
    await subjects.save();
    res.status(200).json(test);

  } catch (err) {
    next(err);
  }

};

exports.getonetestsubject = async (req, res, next) => {
  try {
   const test = await Subject.findById(req.params.testId);
   res.status(200).json(test);
  } catch (err) {
    next(err);
  }

};

exports.replacetestsubject = async (req, res, next) => {
  try {
    const result = await Subject.findByIdAndUpdate(req.params.testId, {
        name: req.body.name,
        type: req.body.type,
        test_note: req.body.test_note,
        test_coefficient:req.body.test_coefficient,  
    });  
   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.updatetestsubject = async (req, res, next) => {
  try {
   const result = await Test.findByIdAndUpdate(req.params.testId, {
    name: req.body.name,
    type: req.body.type,
    test_note: req.body.test_note,
    test_coefficient:req.body.test_coefficient,   
  });

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.deletetestsubject = async (req, res, next) => {
  try {
    const testId = req.params.testId;
   //get test
   const test= await Test.findById(testId);
   if (!test){
     return res.status(404).json({error : 'we can not found this test'});
   }
   console.log(test);
   const subjectId = test.subjects;
   console.log(subjectId);
   // get subject
   const subjects = await Subject.findById(subjectId);
   console.log(subjects);
   //remove test
   await test.remove();
   console.log(test);
   console.log(subjects);
   //remove from subject list
   subjects.tests.pull(test);
   await subjects.save();

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};
