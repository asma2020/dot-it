const express = require('express');
const mongoose = require("mongoose");
//const Quarter = require("../models/Quarters.model");
const Schedule = require("../models/Schedules.model"); 
const Subject = require("../models/Subjects.model");
const Session  = require("../models/Sessions.model");

/************crud simple juste pour tester le front*************/
  exports.getall = (req, res) => {
    Session.find()
    .select("designation salle")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        sessions: docs.map(doc => {
          return {
            designation: doc.designation,
            salle: doc.salle,
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/quarters/get" + doc._id
            }
          };
        })
      };

      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.post = (req, res) => {
  const session = new Session({
    _id: new mongoose.Types.ObjectId(),
    designation: req.body.designation,
    salle: req.body.salle, 
  });
  session
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created Session successfully",
        createdSession: {
            designation: result.designation,
            salle: result.salle,
            _id: result._id,
            request: {
                type: 'GET',
                url: "http://localhost:3000/sessions/post" + result._id
            }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.getone = (req, res) => {
  const id = req.params.sessionId;
  Session.findById(id)
    .select('designation salle')
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
            session: doc,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/sessions/get/:id'
            }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.update = (req, res) => {
// Find Session and update it with the request body
Session.findByIdAndUpdate(req.params.sessionId, {
    designation: req.body.designation,
    salle: req.body.salle,
}, {new: true})
.then(session=> {
    if(!session) {
        return res.status(404).send({
            message: "Session not found with id " + req.params.sessionId
        });
    }
    res.send(session);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "Session not found with id " + req.params.sessionId
        });                
    }
    return res.status(500).send({
        message: "Error updating Session with id " + req.params.sessionId
    });
});
};

exports.delete = (req, res) => {
  const id = req.params.sessionId;
  Session.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Session deleted',
          request: {
              type: 'DELETE',
              url: 'http://localhost:3000/sessions/id',
              body: { designation: 'String', salle: 'Number'}
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
/*********************le code a utiliser ********************/
exports.getAllSchedule= async (req, res, next) => {
  try {
    const sessions = await Session.find({});
    res.status(200).json(sessions);
  } catch (err) {
    next(err);
  }

};

exports.newSession = async (req, res, next) => {
  try {
    // find actuel schedule
    const schedules = await Schedule.findById(req.body.schedules);
    //create a new Session
    const newSession = {
      _id: new mongoose.Types.ObjectId(),
      designation: req.body.designation,
      salle: req.body.salle,
    };

    delete newSession.schedules;
    const session = new Session(newSession);
    session.schedules = schedules;
    await session.save();
    // add the newly created session to the actuel schedule year
    schedules.sessions.push(session);
    await schedules.save();
    res.status(200).json(session);

  } catch (err) {
    next(err);
  }

};

exports.getonesessionschedule = async (req, res, next) => {
  try {
   const quarter = await Session.findById(req.params.quarterId);
   res.status(200).json(quarter);
  } catch (err) {
    next(err);
  }

};

exports.replacesessionschedule = async (req, res, next) => {
  try {
    const result = await Session.findByIdAndUpdate(req.params.sessionId, {
        designation: req.body.designation,
        salle: req.body.salle,
    });  
   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.updatesessionschedule = async (req, res, next) => {
  try {
   const result = await Session.findByIdAndUpdate(req.params.sessionId, {
    designation: req.body.designation,
    salle: req.body.salle,
  });

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.deletesessionschedule = async (req, res, next) => {
  try {
    const sessionId = req.params.sessionId;
   //get session
   const session= await Session.findById(sessionId);
   if (!session){
     return res.status(404).json({error : 'we can not found this session'});
   }
   console.log(session);
   const scheduleId = session.schedules;
   console.log(scheduleId);
   // get schedule
   const schedules = await Schedule.findById(scheduleId);
   console.log(schedules);
   //remove session 
   await session.remove();
   console.log(session);
   console.log(schedules);
   //remove from schedule list
   schedules.sessions.pull(session);
   await schedules.save();

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

/**************get & post Subject from Sessions ************* */
exports.getSessionSubject= async(req, res, next) => {
  try {
  const {sessionId} = req.params;
  const session= await Session.findById(sessionId).populate('subjects');
  res.status(200).json(session.subjects)
  } catch (err) {
  next(err);
}
};


exports.newSessionSubject= async(req, res, next) => {
  try {
  const {sessionId} = req.params;
  const newSubject = new Subject({
    designation: req.body.designation,
    coefficient: req.body.coefficient,
    photo:req.file.path,
    subject_student_average:req.body.subject_student_average, 
  });
  //get session
  const session = await Subject.findById(sessionId);
  //assign Subject to session
  newSubject.subjects = session;
  //save the event
   await newSubject.save();
  // add Subject to sessions
  session.subjects.push(newSubject);
  //save the session
   await session.save();

  res.status(200).json(newSubject);
} catch (err) {
  next(err);
}
};


/**************get & post Subjects from quarter************* */
/*exports.getQuarterSubject= async(req, res, next) => {
    try {
    const {quarterId} = req.params;
    const quarter= await Quarter.findById(quarterId).populate('subjects');
    res.status(200).json(quarter.subjects)
    } catch (err) {
    next(err);
  }
  };
  
  
  exports.newQuarterSubject= async(req, res, next) => {
    try {
    const {quarterId} = req.params;
    //const id = req.params.quarterId;
    const newSubject = new Subject({
      _id: new mongoose.Types.ObjectId(),
      designation: req.body.designation,
      coefficient: req.body.coefficient,
      photo: req.file.path,
      subject_student_average:req.body.subject_student_average,      
    });
    //get quarter
    const quarter = await Quarter.findById(quarterId);
    //assign subject to quarter
    newSubject.subjects = quarter;
    //save the event
     await newSubject.save();
    // add subject to quarters
    quarter.subjects.push(newSubject);
    //save the quarter
     await quarter.save();
  
    res.status(200).json(newSubject);
  } catch (err) {
    next(err);
  }
  };*/
