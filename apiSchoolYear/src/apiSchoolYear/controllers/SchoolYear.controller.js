const express = require('express');
const mongoose = require("mongoose");
const Event = require("../models/Events.model");
const Vacation = require("../models/Vacations.model");
const SchoolYear = require("../models/SchoolYears.model");
const Subject = require("../models/Subjects.model");
const Quarter = require("../models/Quarters.model");
const Test = require("../models/Tests.model");
const User = require("../models/User");
const axios = require("axios");

  exports.getall = (req, res,next) => {
    SchoolYear.find()
    .select("year description photo  general_students_average")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        schoolYears: docs.map(doc => {
          return {
            year: doc.year,
            description: doc.description,
            photo: doc.photo, 
            general_students_average: doc.general_students_average,
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/schoolYears/get" + doc._id
            }
          };
        })
      };
      /*await axios.post('http://localhost:4005/events', {
        type: 'Registration',
        data: {
          year,
          description,
          photo,
          general_students_average,
        },
      });*/
      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.post = (req, res,next) => {
  const schoolYear = new SchoolYear({
    _id: new mongoose.Types.ObjectId(),
    year:req.body.year,
    description: req.body.description,
    photo: req.file.path, 
    general_students_average: req.body.general_students_average,
  });
  schoolYear
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created SchoolYear successfully",
        createdSchoolYear: {
            year:result.year,
            description: result.description,
            general_students_average: result.general_students_average,
            photo: result.photo,
            _id: result._id,
            request: {
                type: 'GET',
                url: "http://localhost:3000/schoolYears/post" + result._id
            }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.getone = (req, res,next) => {
  const id = req.params.schoolYearId;
  SchoolYear.findById(id)
    .select('year description photo  general_students_average')
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
          schoolYear: doc,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/schoolYears/get/:id'
            }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.update = (req, res,next) => {
// Find SchoolYear and update it with the request body
SchoolYear.findByIdAndUpdate(req.params.schoolYearId, {
    year:req.body.year,
    description: req.body.description,
    photo: req.file.path, 
    general_students_average: req.body.general_students_average,
}, {new: true})
.then(schoolYear=> {
    if(!schoolYear) {
        return res.status(404).send({
            message: "SchoolYear not found with id " + req.params.schoolYearId
        });
    }
    res.send(schoolYear);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "schoolYear not found with id " + req.params.schoolYearId
        });                
    }
    return res.status(500).send({
        message: "Error updating schoolYear with id " + req.params.schoolYearId
    });
});
};

exports.delete = (req, res,next) => {
  const id = req.params.schoolYearId;
  SchoolYear.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'SchoolYear deleted',
          request: {
              type: 'DELETE',
              url: 'http://localhost:3000/schoolYears/id',
              body: { year: 'String', description: 'String',   photo: 'String', general_students_average:'Number'}
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

/********event*************/
exports.getSchoolYearEvent= async(req, res, next) => {
  try {
  const {schoolYearId} = req.params;
  const schoolYear= await SchoolYear.findById(schoolYearId).populate('events');
  res.status(200).json(schoolYear.events)
  } catch (err) {
  next(err);
}
};


exports.newSchoolYearEvent= async(req, res, next) => {
  try {
  const {schoolYearId} = req.params;
  //const id = req.params.schoolYearId;
  const newEvent = new Event({
    _id: new mongoose.Types.ObjectId(),
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    description:req.body.description,
    photo: req.file.path,
  });
  //get school year
  const schoolYear= await SchoolYear.findById(schoolYearId);
  //assign event to school year
  newEvent.events=schoolYear;
  //save the event
   await newEvent.save();
  // add event to school's years
  schoolYear.events.push(newEvent);
  //save the school year
   await schoolYear.save();

  res.status(200).json(newEvent);
} catch (err) {
  next(err);
}
};
/******************************vacance****************************/
exports.getSchoolYearVacance= async(req, res, next) => {
  try {
  const {schoolYearId} = req.params;
  const schoolYear= await SchoolYear.findById(schoolYearId).populate('vacations');
  res.status(200).json(schoolYear.vacations)
  } catch (err) {
  next(err);
}
};


exports.newSchoolYearVacation= async(req, res, next) => {
  try {
  const {schoolYearId} = req.params;
  //const id = req.params.schoolYearId;
  const newVacation = new Vacation({
    _id: new mongoose.Types.ObjectId(),
    year: req.body.year,
    description: req.body.description,
    photo: req.file.path,
    general_students_average: req.body.general_students_average,
 
  });
  //get school year
  const schoolYear= await SchoolYear.findById(schoolYearId);
  //assign vacations to school year
  newVacation.vacations=schoolYear;
  //save the vacation
   await newVacation.save();
  // add vacation to school's years
  schoolYear.vacations.push(newVacation);
  //save the school year
   await schoolYear.save();

  res.status(200).json(newVacation);
} catch (err) {
  next(err);
}
};
/******************************quarters****************************/
//get  quarter from school year 
exports.getSchoolYearQuarter= async(req, res, next) => {
  try {
  const {schoolYearId} = req.params;
  const schoolYear= await SchoolYear.findById(schoolYearId).populate('quarters');
  res.status(200).json(schoolYear.quarters.subjects)
  } catch (err) {
  next(err);
}
};
//get  subject from school year 
exports.getSubjectSchoolYear= async(req, res, next) => {
  try {
    const schoolYear = await Quarter.findById(req.body.schoolYears);
    const {quarterId} = req.params;
  const quarter= await Quarter.findById(quarterId).populate('subjects');
  res.status(200).json(quarter.subjects)
  } catch (err) {
  next(err);
}
};


//get test from school year 
exports.getTestSchoolYear= async(req, res, next) => {
  try {

    const {subjectId} = req.params;
    const schoolYear = await Quarter.findById(req.body.schoolYears);
    const quarter = await Subject.findById(req.body.quarters);
    const subject= await Subject.findById(subjectId).populate('tests');
  res.status(200).json(subject.tests)
  } catch (err) {
  next(err);
}
};

/*exports.getTestSchoolYear= async(req, res, next) => {
  try {
    const {subjectId} = req.params;
    const schoolYear = await Quarter.findById(req.body.schoolYears).findById(req.body.subjectId).populate('tests');
  res.status(200).json(subject.tests)
  } catch (err) {
  next(err);
}
};*/


exports.newSchoolYearQuarter= async(req, res, next) => {
  try {
  const {schoolYearId} = req.params;
  //const id = req.params.schoolYearId;
  const newQuarter = new Quarter({
    _id: new mongoose.Types.ObjectId(),
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    description: req.body.description,
    student_quarterly_average: req.body.student_quarterly_average,
 
  });
  //get school year
  const schoolYear= await SchoolYear.findById(schoolYearId);
  //assign quarters to school year
  newQuarter.quarters=schoolYear;
  //save the quarter
   await newQuarter.save();
  // add quarter to school's years
  schoolYear.quarters.push(newQuarter);
  //save the school year
   await schoolYear.save();

  res.status(200).json(newQuarter);
} catch (err) {
  next(err);
}
};
/********parent*************/
exports.getSchoolYearUser= async(req, res, next) => {
  try {
  const {schoolYearId} = req.params;
  const schoolYear= await SchoolYear.findById(schoolYearId).populate('users');
  res.status(200).json(schoolYear.users)
  } catch (err) {
  next(err);
}
};


exports.newSchoolYearUser= async(req, res, next) => {
  try {
  const {schoolYearId} = req.params;
  const newUser= new User({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name, 
    email: req.body.email,
    password: req.body.password,
    role: req.body.role,
    registrationNumber: req.body.registrationNumber,
    birthdate: req.body.birthdate,
    phoneNumber: req.body.phoneNumber,
    address: req.body.address,
    salary: req.body.salary,
    gender: req.body.gender,
    category: req.body.category,
    contractDueDate: req.body.contractDueDate,
    contractType: req.body.contractType,
    nic: req.body.nic,
    status: req.body.status,
    photo: req.body.photo,
    school_level: req.body.school_level,
    last_school: req.body.last_school,
    study_fees: req.body.study_fees,
    schoolYears: req.body.schoolYears,
    quarters: req.body.quarters,
    classes: req.body.classes,
    schedules: req.body.schedules,
    sessions: req.body.sessions,
    subjects: req.body.subjects,
    tests: req.body.tests,
  });
  console.log(newUser);
  //get school year
  const schoolYear= await SchoolYear.findById(schoolYearId);
  //assign User to school year
  newUser.users=schoolYear;
  //save the user
   await newUser.save();
  // add user to school's years
  schoolYear.users.push(newUser);
  //save the school year
   await schoolYear.save();

  res.status(200).json(newUser);
} catch (err) {
  next(err);
}
};

