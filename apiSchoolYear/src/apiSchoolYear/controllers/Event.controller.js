const express = require('express');
const mongoose = require("mongoose");
const Event = require("../models/Events.model");
const SchoolYear = require("../models/SchoolYears.model"); 

/************crud simple juste pour tester le front*************/
  exports.getall = (req, res) => {
    Event.find()
    .select("designation start_date end_date description  photo")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        events: docs.map(doc => {
          return {
            designation: doc.designation,
            start_date: doc.start_date,
            description: doc.description,
            photo: doc.photo, 
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/events/get" + doc._id
            }
          };
        })
      };

      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.post = (req, res) => {
  const event = new Event({
    _id: new mongoose.Types.ObjectId(),
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    description:req.body.description,
    photo: req.file.path,
  });
  event
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created Event successfully",
        createdEvent: {
            designation: result.designation,
            start_date: result.start_date,
            end_date: result.end_date,
            description:result.description,
            _id: result._id,
            request: {
                type: 'GET',
                url: "http://localhost:3000/events/post" + result._id
            }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.getone = (req, res) => {
  const id = req.params.eventId;
  Event.findById(id)
    .select('designation start_date end_date description  photo')
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
          event: doc,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/events/get/:id'
            }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.update = (req, res) => {
// Find Event and update it with the request body
Event.findByIdAndUpdate(req.params.eventId, {
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    description:req.body.description,
    photo: req.file.path,
}, {new: true})
.then(event=> {
    if(!event) {
        return res.status(404).send({
            message: "Event not found with id " + req.params.eventId
        });
    }
    res.send(event);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "event not found with id " + req.params.eventId
        });                
    }
    return res.status(500).send({
        message: "Error updating event with id " + req.params.eventId
    });
});
};

exports.delete = (req, res) => {
  const id = req.params.eventId;
  Event.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Event deleted',
          request: {
              type: 'DELETE',
              url: 'http://localhost:3000/events/id',
              body: { designation: 'String',  start_date: 'Date', end_date:'Date',  description: 'String',   photo: 'String'}
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
/********************le code a utiliser *********************/
exports.getAllSchool= async (req, res, next) => {
  try {
    const events = await Event.find({});
    res.status(200).json(events);
  } catch (err) {
    next(err);
  }

};

exports.newEvent = async (req, res, next) => {
  try {
    // find actuel school year
    const schoolYears = await SchoolYear.findById(req.body.schoolYears);
    
    //create a new event
    const newEvent = {
      _id: new mongoose.Types.ObjectId(),
      designation: req.body.designation,
      start_date: req.body.start_date,
      end_date: req.body.end_date,
      description:req.body.description,
      photo: req.file.path,
    };
    delete newEvent.schoolYears;

    console.log("event "+newEvent)
    const event = new Event(newEvent);
    event.schoolYears = schoolYears;
    await event.save();
    // add the newly created event to the actuel school year
    schoolYears.events.push(event);
    await schoolYears.save();

    
    res.status(200).json(event);

  } catch (err) {
    next(err);
  }

};

exports.getoneeventschool = async (req, res, next) => {
  try {
   const event = await Event.findById(req.params.eventId);
   res.status(200).json(event);
  } catch (err) {
    next(err);
  }

};

exports.replaceeventschool = async (req, res, next) => {
  try {
    const result = await Event.findByIdAndUpdate(req.params.eventId, {
      designation: req.body.designation,
      start_date: req.body.start_date,
      end_date: req.body.end_date,
      description:req.body.description,
      photo: req.file.path,
    });  
   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.updateeventschool = async (req, res, next) => {
  try {
   const result = await Event.findByIdAndUpdate(req.params.eventId, {
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    description:req.body.description,
    photo: req.file.path,
  });

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.deleteeventschool = async (req, res, next) => {
  try {
    const eventId = req.params.eventId;
   //get event
   const event= await Event.findById(eventId);
   if (!event){
     return res.status(404).json({error : 'we can not found this event'});
   }
   console.log(event);
   //const event = await Event.findById(req.params.eventId);
   const schoolYearId = event.schoolYears;
   console.log(schoolYearId);
   // get school Year
   const schoolYears = await SchoolYear.findById(schoolYearId);
   console.log(schoolYears);
   //remove event 
   await event.remove();
   console.log(event);
   console.log(schoolYears);
   //remove from schoolYear list
   schoolYears.events.pull(event);
   await schoolYears.save();

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};