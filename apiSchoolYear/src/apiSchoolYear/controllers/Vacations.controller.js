const express = require('express');
const mongoose = require("mongoose");
const Vacation = require("../models/Vacations.model");
const SchoolYear = require("../models/SchoolYears.model"); 

/***********CRUD testing************* */
  exports.getall = (req, res) => {
    Vacation.find()
    .select("designation start_date end_date type photo")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        vacations: docs.map(doc => {
          return {
            designation: doc.designation,
            start_date: doc.start_date,
            end_date: doc.end_date,
            type: doc.type,
            photo: doc.photo,
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/vacations/get" + doc._id
            }
          };
        })
      };

      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.post = (req, res) => {
  const vacation = new Vacation({
    _id: new mongoose.Types.ObjectId(),
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    type:req.body.type,
    photo: req.file.path,
  });
  vacation
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created Vacation successfully",
        createdVacation: {
            designation: result.designation,
            start_date: result.start_date,
            end_date: result.end_date,
            type: result.type,
            _id: result._id,
            request: {
                type: 'GET',
                url: "http://localhost:3000/vacations/post" + result._id
            }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.getone = (req, res) => {
  const id = req.params.vacationId;
  Vacation.findById(id)
    .select('designation start_date end_date type photo')
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
          vacation: doc,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/vacations/get/:id'
            }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.update = (req, res) => {
// Find Vacation and update it with the request body
Vacation.findByIdAndUpdate(req.params.vacationId, {
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    type:req.body.type,
    photo: req.file.path,
}, {new: true})
.then(vacation=> {
    if(!vacation) {
        return res.status(404).send({
            message: "Vacation not found with id " + req.params.vacationId
        });
    }
    res.send(vacation);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "Vacation not found with id " + req.params.vacationId
        });                
    }
    return res.status(500).send({
        message: "Error updating Vacation with id " + req.params.vacationId
    });
});
};

exports.delete = (req, res) => {
  const id = req.params.vacationId;
  Vacation.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Vacation deleted',
          request: {
              type: 'DELETE',
              url: 'http://localhost:3000/vacations/id',
              body: { designation: 'String',  start_date: 'Date',end_date: 'Date',  type: 'String',   photo: 'String'}
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
/***************AppCrud**************************/
exports.getAllSchool= async (req, res, next) => {
  try {
    const vacations = await Vacation.find({});
    res.status(200).json(vacations);
  } catch (err) {
    next(err);
  }

};

exports.newVacation = async (req, res, next) => {
  try {
    // find actuel school year
    const schoolYears = await SchoolYear.findById(req.body.schoolYears);
    //create a new vacation
    const newVacation = {
      _id: new mongoose.Types.ObjectId(),
      designation: req.body.designation,
      start_date: req.body.start_date,
      end_date: req.body.end_date,
      type:req.body.type,
      photo: req.file.path,
    };
    delete newVacation.schoolYears;
    const vacation = new Vacation(newVacation);
    vacation.schoolYears = schoolYears;
    await vacation.save();
    // add the newly created vacation to the actuel school year
    schoolYears.vacations.push(vacation);
    await schoolYears.save();
    res.status(200).json(vacation);

  } catch (err) {
    next(err);
  }

};

exports.getonevacationschool = async (req, res, next) => {
  try {
   const vacation = await Vacation.findById(req.params.vacationId);
   res.status(200).json(vacation);
  } catch (err) {
    next(err);
  }

};

exports.replacevacationschool = async (req, res, next) => {
  try {
    const result = await Vacation.findByIdAndUpdate(req.params.vacationId, {
      designation: req.body.designation,
      start_date: req.body.start_date,
      end_date: req.body.end_date,
      type:req.body.type,
      photo: req.file.path,
    });  
   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.updatevacationschool = async (req, res, next) => {
  try {
   const result = await Vacation.findByIdAndUpdate(req.params.vacationId, {
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    type:req.body.type,
    photo: req.file.path,
  });

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.deletevacationschool = async (req, res, next) => {
  try {
    const vacationId = req.params.vacationId;
   //get vacation
   const vacation= await Vacation.findById(vacationId);
   if (!vacation){
     return res.status(404).json({error : 'we can not found this vacation'});
   }
   console.log(vacation);
   //const vacation = await Vacation.findById(req.params.vacationId);
   const schoolYearId = vacation.schoolYears;
   console.log(schoolYearId);
   // get school Year
   const schoolYears = await SchoolYear.findById(schoolYearId);
   console.log(schoolYears);
   //remove vacation 
   await vacation.remove();
   console.log(vacation);
   console.log(schoolYears);
   //remove from schoolYear list
   schoolYears.vacations.pull(vacation);
   await schoolYears.save();

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

