const express = require('express');
const mongoose = require("mongoose");
//const Quarter = require("../models/Quarters.model");
const Test = require("../models/Tests.model"); 
const Subject = require("../models/Subjects.model");
const Session  = require("../models/Sessions.model");

/***********CRUD testing************* */
  exports.getall = (req, res) => {
    Subject.find()
    .select("designation coefficient subject_student_average")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        subjects: docs.map(doc => {
          return {
            designation: doc.designation,
            coefficient: doc.coefficient,
            //photo: doc.photo,
            subject_student_average: doc.subject_student_average,  
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/subjects/get" + doc._id
            }
          };
        })
      };

      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.post = (req, res) => {
  const subject = new Subject({
    _id: new mongoose.Types.ObjectId(),
    designation: req.body.designation,
    coefficient: req.body.coefficient,
    //photo: req.file.path,
    subject_student_average:req.body.subject_student_average, 
  });
  subject
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created Subject successfully",
        createdSubject: {
            designation: result.designation,
            coefficient: result.coefficient,
            //photo: result.path, 
            subject_student_average:result.subject_student_average,
            _id: result._id,
            request: {
                type: 'GET',
                url: "http://localhost:3000/subjects/post" + result._id
            }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.getone = (req, res) => {
  const id = req.params.subjectId;
  Subject.findById(id)
    .select('designation coefficient subject_student_average')
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
            subject: doc,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/subjects/get/:id'
            }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.update = (req, res) => {
// Find Subject and update it with the request body
Subject.findByIdAndUpdate(req.params.subjectId, {
    designation: req.body.designation,
    coefficient: req.body.coefficient,
    //photo: req.file.path,
    subject_student_average:req.body.subject_student_average, 
}, {new: true})
.then(subject => {
    if(!subject) {
        return res.status(404).send({
            message: "Subject not found with id " + req.params.subjectId
        });
    }
    res.send(subject);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "Subject not found with id " + req.params.subjectId
        });                
    }
    return res.status(500).send({
        message: "Error updating Subject with id " + req.params.subjectId
    });
});
};

exports.delete = (req, res) => {
  const id = req.params.subjectId;
  Subject.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Subject deleted',
          request: {
              type: 'DELETE',
              url: 'http://localhost:3000/subjects/id',
              body: { designation: 'String',  coefficient: 'Number', /*photo: 'String',*/ subject_student_average: 'Number'}
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
/*****************le code a utiliser ************************/
exports.getAllSession= async (req, res, next) => {
  try {
    const subjects = await Subject.find({});
    res.status(200).json(subjects);
  } catch (err) {
    next(err);
  }

};


exports.newSubject = async (req, res, next) => {
  try {
    // find actuel session
    const sessions = await Session.findById(req.body.sessions);
    //create a new Subject
    const newSubject = {
      _id: new mongoose.Types.ObjectId(),
      designation: req.body.designation,
      coefficient: req.body.coefficient,
      //photo: req.file.path,
      subject_student_average:req.body.subject_student_average, 
    };
    delete newSubject.sessions;
    const subject = new Subject(newSubject);
    subject.sessions = sessions;
    await subject.save();
    // add the newly created subject to the actuel session
    sessions.subjects.push(subject);
    await sessions.save();
    res.status(200).json(subject);

  } catch (err) {
    next(err);
  }

};



exports.getonesubjectsession = async (req, res, next) => {
  try {
   const subject = await Session.findById(req.params.subjectId);
   res.status(200).json(subject);
  } catch (err) {
    next(err);
  }

};

exports.replacesubjectsession = async (req, res, next) => {
  try {
    const result = await Session.findByIdAndUpdate(req.params.subjectId, {
      designation: req.body.designation,
      coefficient: req.body.coefficient,
      //photo: req.file.path,
      subject_student_average:req.body.subject_student_average, 
    });  
   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.updatesubjectsession = async (req, res, next) => {
  try {
   const result = await Subject.findByIdAndUpdate(req.params.subjectId, {
    designation: req.body.designation,
    coefficient: req.body.coefficient,
    //photo: req.file.path,
    subject_student_average:req.body.subject_student_average, 
  });

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.deletesubjectsession = async (req, res, next) => {
  try {
    const subjectId = req.params.subjectId;
   //get subject
   const subject= await Subject.findById(subjectId);
   if (!subject){
     return res.status(404).json({error : 'we can not found this subject'});
   }
   console.log(subject);
   const sessionId = subject.sessions;
   console.log(sessionId);
   // get session
   const sessions = await Session.findById(sessionId);
   console.log(sessions);
   //remove subject
   await subject.remove();
   console.log(subject);
   console.log(sessions);
   //remove from session list
   sessions.subjects.pull(subject);
   await sessions.save();

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};
/**************get & post  Tests from Subjects ************* */
exports.getSubjectTest= async(req, res, next) => {
  try {
  const {subjectId} = req.params;
  const subject= await Subject.findById(subjectId).populate('tests');
  res.status(200).json(subject.tests)
  } catch (err) {
  next(err);
}
};


exports.newSubjectTest= async(req, res, next) => {
  try {
  const {subjectId} = req.params;
  //const id = req.params.subjectId;
  const newTest = new Test({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    type: req.body.type,
    test_note: req.body.test_note,
    test_coefficient:req.body.test_coefficient,      
  });
  //get subject
  const subject = await Subject.findById(subjectId);
  //assign test to subject
  newTest.tests = subject;
  //save the subject
   await newTest.save();
  // add test to subject
  subject.tests.push(newTest);
  //save the subject
   await subject.save();

  res.status(200).json(newTest);
} catch (err) {
  next(err);
}
};
