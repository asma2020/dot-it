const express = require('express');
const mongoose = require("mongoose");
const Classe = require("../models/Classes.model");
const Session = require("../models/Sessions.model"); 
const Schedule = require("../models/Schedules.model");
/************crud simple juste pour tester le front*************/
  exports.getall = (req, res) => {
    Schedule.find()
    .select("designation start_date end_date description photo")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        schedules: docs.map(doc => {
          return {
            designation: doc.designation,
            start_date: doc.start_date,
            end_date: doc.end_date,
            description: doc.description,
            photo: doc.path,
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/schedules/get" + doc._id
            }
          };
        })
      };

      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.post = (req, res) => {
  const schedule = new Schedule({
    _id: new mongoose.Types.ObjectId(),
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    description: req.body.description,
    photo: req.file.path,
  });
  schedule
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created Schedulesuccessfully",
        createdSchedule: {
           designation: result.designation,
            start_date: result.start_date,
            end_date: result.end_date,
            description: result.description,
            photo: result.path,
            _id: result._id,
            request: {
                type: 'GET',
                url: "http://localhost:3000/schedules/post" + result._id
            }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.getone = (req, res) => {
  const id = req.params.scheduleId;
  Schedule.findById(id)
    .select('designation start_date end_date description photo')
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
            schedule: doc,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/schedules/get/:id'
            }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.update = (req, res) => {
// Find Schedule and update it with the request body
Classe.findByIdAndUpdate(req.params.scheduleId, {
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    description: req.body.description,
    photo: req.file.path, 
}, {new: true})
.then(schedule=> {
    if(!schedule) {
        return res.status(404).send({
            message: "Schedule not found with id " + req.params.scheduleId
        });
    }
    res.send(schedule);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "Schedule not found with id " + req.params.scheduleId
        });                
    }
    return res.status(500).send({
        message: "Error updating Schedule with id " + req.params.scheduleId
    });
});
};

exports.delete = (req, res) => {
  const id = req.params.scheduleId;
  Schedule.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Schedule deleted',
          request: {
              type: 'DELETE',
              url: 'http://localhost:3000/schedules/id',
              body: { designation: 'String', start_date: 'Date', end_date: 'Date', description: 'String', photo: 'String'}
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
/******************le code a utiliser ***********************/
exports.getAllClasse = async (req, res, next) => {
  try {
    const schedules = await Schedule.find({});
    res.status(200).json(schedules);
  } catch (err) {
    next(err);
  }

};

exports.newSchedule = async (req, res, next) => {
  try {
    // find actuel classe
    const classes = await Classe .findById(req.body.classes);
    //create a new Schedule
    const newSchedule = {
      _id: new mongoose.Types.ObjectId(),
      designation: req.body.designation,
      start_date: req.body.start_date,
      end_date: req.body.end_date,
      description: req.body.description,
      photo: req.file.path,
    };

    delete newSchedule.classes;
    const schedule = new Schedule(newSchedule);
    schedule.classes = classes;
    await schedule.save();
    // add the newly created schedule to the actuel classe
    classes.schedules.push(schedule);
    await classes.save();
    res.status(200).json(schedule);

  } catch (err) {
    next(err);
  }

};

exports.getonescheduleclasse= async (req, res, next) => {
  try {
   const schedule = await Schedule.findById(req.params.scheduleId);
   res.status(200).json(schedule);
  } catch (err) {
    next(err);
  }

};

exports.replacescheduleclasse = async (req, res, next) => {
  try {
    const result = await Schedule.findByIdAndUpdate(req.params.scheduleId, {
        designation: req.body.designation,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
        description: req.body.description,
        photo: req.file.path, 
    });  
   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.updatescheduleclasse = async (req, res, next) => {
  try {
   const result = await Schedule.findByIdAndUpdate(req.params.scheduleId, {
    designation: req.body.designation,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    description: req.body.description,
    photo: req.file.path,
  });

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.deletescheduleclasse = async (req, res, next) => {
  try {
    const scheduleId = req.params.scheduleId;
   //get schedule
   const schedule= await Schedule.findById(scheduleId);
   if (!schedule){
     return res.status(404).json({error : 'we can not found this schedule'});
   }
   console.log(schedule);
   const classeId = schedule.classes;
   console.log(classeId);
   // get classe
   const classes = await Classe.findById(classeId);
   console.log(classes);
   //remove schedule
   await schedule.remove();
   console.log(schedule);
   console.log(classes);
   //remove from classe list
   classes.schedules.pull(schedule);
   await classes.save();

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};


/**************get & post Schedules from schedule************* */
exports.getSessionSchedule= async(req, res, next) => {
    try {
    const {scheduleId} = req.params;
    const schedule= await Schedule.findById(scheduleId).populate('sessions');
    res.status(200).json(schedule.sessions)
    } catch (err) {
    next(err);
  }
  };
  
  
  exports.newSessionSchedule= async(req, res, next) => {
    try {
    const {scheduleId} = req.params;
    const newSession = new Session({
      designation: req.body.designation,
      salle: req.body.salle,
    });
    //get schedule
    const schedule = await Schedule.findById(scheduleId);
    //assign Session to schedule
    newSession.schedules = schedule;
    //save the event
     await newSession.save();
    // add Session to schedules
    schedule.schedules.push(newSession);
    //save the schedule
     await schedule.save();
  
    res.status(200).json(newSession);
  } catch (err) {
    next(err);
  }
  };

