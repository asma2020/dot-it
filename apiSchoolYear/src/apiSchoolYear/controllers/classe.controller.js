const express = require('express');
const mongoose = require("mongoose");
const Quarter = require("../models/Quarters.model");
const Classe = require("../models/Classes.model"); 
const Schedule = require("../models/Schedules.model");

/************crud simple juste pour tester le front*************/
  exports.getall = (req, res) => {
    Classe.find()
    .select("designation student_numbers division")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        classes: docs.map(doc => {
          return {
            designation: doc.designation,
            student_numbers: doc.student_numbers,
            division: doc.division,
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/classes/get" + doc._id
            }
          };
        })
      };

      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.post = (req, res) => {
  const classe = new Classe({
    _id: new mongoose.Types.ObjectId(),
    designation: req.body.designation,
    student_numbers: req.body.student_numbers,
    division: req.body.division,
  });
  classe
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created Classe successfully",
        createdClasse: {
            designation: result.designation,
            student_numbers: result.student_numbers,
            division: result.division,
            _id: result._id,
            request: {
                type: 'GET',
                url: "http://localhost:3000/classes/post" + result._id
            }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

  exports.getone = (req, res) => {
  const id = req.params.classeId;
  Classe.findById(id)
    .select('designation student_numbers division')
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
            classe: doc,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/classes/get/:id'
            }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.update = (req, res) => {
// Find Classe and update it with the request body
Quarter.findByIdAndUpdate(req.params.classeId, {
    designation: req.body.designation,
    student_numbers: req.body.student_numbers,
    division: req.body.division,    
}, {new: true})
.then(classe=> {
    if(!classe) {
        return res.status(404).send({
            message: "Classe not found with id " + req.params.classeId
        });
    }
    res.send(classe);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "Classe not found with id " + req.params.classeId
        });                
    }
    return res.status(500).send({
        message: "Error updating Classe with id " + req.params.classeId
    });
});
};

exports.delete = (req, res) => {
  const id = req.params.classeId;
  Classe.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Classe deleted',
          request: {
              type: 'DELETE',
              url: 'http://localhost:3000/classes/id',
              body: { designation: 'String', student_numbers: 'Number', division: 'String'}
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
/********************le code a utiliser *********************/
exports.getAllQuarter = async (req, res, next) => {
  try {
    const classes = await Classe.find({});
    res.status(200).json(classes);
  } catch (err) {
    next(err);
  }

};

exports.newClasse = async (req, res, next) => {
  try {
    // find actuel quarter
    const quarters = await Quarter .findById(req.body.quarters);
    //create a new Classe
    const newClasse = {
      _id: new mongoose.Types.ObjectId(),
      designation: req.body.designation,
      student_numbers: req.body.student_numbers,
      division: req.body.division,  
    };

    delete newClasse.quarters;
    const classe = new Classe(newClasse);
    classe.quarters = quarters;
    await classe.save();
    // add the newly created classe to the actuel quarter
    quarters.classes.push(classe);
    await quarters.save();
    res.status(200).json(classe);

  } catch (err) {
    next(err);
  }

};

exports.getoneclassequarter = async (req, res, next) => {
  try {
   const classe = await Classe.findById(req.params.classeId);
   res.status(200).json(classe);
  } catch (err) {
    next(err);
  }

};

exports.replaceclassequarter = async (req, res, next) => {
  try {
    const result = await Classe.findByIdAndUpdate(req.params.classeId, {
        designation: req.body.designation,
        student_numbers: req.body.student_numbers,
        division: req.body.division,  
    });  
   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.updateclassequarter = async (req, res, next) => {
  try {
   const result = await Classe.findByIdAndUpdate(req.params.classeId, {
    designation: req.body.designation,
    student_numbers: req.body.student_numbers,
    division: req.body.division,  
  });

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};

exports.deleteclassequarter = async (req, res, next) => {
  try {
    const classeId = req.params.classeId;
   //get classe
   const classe= await Classe.findById(classeId);
   if (!classe){
     return res.status(404).json({error : 'we can not found this classe'});
   }
   console.log(classe);
   const quarterId = classe.quarters;
   console.log(quarterId);
   // get quarter
   const quarters = await Quarter .findById(quarterId);
   console.log(quarters);
   //remove classe
   await classe.remove();
   console.log(classe);
   console.log(quarters);
   //remove from quarter list
   quarters.classes.pull(classe);
   await quarters.save();

   res.status(200).json({succes:true});
  } catch (err) {
    next(err);
  }

};
/**************get & post Schedules from classe************* */
exports.getClasseSchedule= async(req, res, next) => {
    try {
    const {classeId} = req.params;
    const classe= await Classe.findById(classeId).populate('schedules');
    res.status(200).json(classe.schedules)
    } catch (err) {
    next(err);
  }
  };
  
  
  exports.newClasseSchedule= async(req, res, next) => {
    try {
    const {classeId} = req.params;
    //const id = req.params.classeId;
    const newSchedule = new Schedule({
      designation: req.body.designation,
      start_date: req.body.start_date,
      end_date: req.body.end_date,
      description: req.body.description,
      photo: req.file.path,
    });
    //get classe
    const classe = await Classe.findById(classeId);
    //assign Schedule to classe
    newSchedule.schedules = classe;
    //save the event
     await newSchedule.save();
    // add Schedule to classes
    classe.schedules.push(newSchedule);
    //save the classe
     await classe.save();
  
    res.status(200).json(newSchedule);
  } catch (err) {
    next(err);
  }
  };
