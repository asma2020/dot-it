const Students = require('../models/Students.model.js');
const mongoose = require('mongoose'), Schema = mongoose.Schema;

const ParentsSchema = mongoose.Schema({
    last_name: { type: String, required: true },
    first_name: { type: String, required: true },
    gender: { type: String, required: true },
    registration_number: { type: Number, required: true },
    address: { type: String, required: true },
    phone: { type: Number, required: true },
    email: { type: String, min: (5, 'Too short, min is 5 characters'),
    max: (32, 'Too long, max is 32 characters'),
    unique: true,
    lowercase: true,
    match: (/^w+((.-)?w+)*@w+((.-)?w+)*(.w{2,3})+$/), required: true },
    login: { type: String, required: true },
    password: { type: String, required: true },
    job: { type: String, required: false },
    nic: { type: Number, required: false },
    photo: { type: String, required: false },
    students:[{
        type:Schema.Types.ObjectId,
        ref:"Students"
    }]
}, {
    timestamps: true
});

module.exports = mongoose.model('Parents', ParentsSchema);

