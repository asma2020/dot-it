const Classes = require('../models/Classes.model.js');
const Sessions = require('../models/Sessions.model.js');
//const Students = require('../models/Students.model.js');
const mongoose = require('mongoose');Schema = mongoose.Schema;

const SchedulesSchema = mongoose.Schema({
    designation: { type: String, required: true },
    start_date: { type: Date, required: true },
    end_date: { type: Date, required: false },
    description: { type: String, required: false },
    photo: { type: String, required: false },
    classes:[{
        type:Schema.Types.ObjectId,
        ref:"Classes"
    }],
    sessions:[{
        type:Schema.Types.ObjectId,
        ref:"Sessions"
    }],
    /*students:[{
        type:Schema.Types.ObjectId,
        ref:"Students"
    }]*/
}, {
    timestamps: true
});

module.exports = mongoose.model('Schedules', SchedulesSchema);

