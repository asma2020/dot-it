const Quarters = require('../models/Quarters.model.js');
const User = require('../models/User.js');
const Vacations = require('../models/Vacations.model.js');
const Events= require('../models/Events.model.js');
const mongoose = require('mongoose'), Schema = mongoose.Schema;


const SchoolYearsSchema = mongoose.Schema({
    year: { type: String, required: true },
    description: { type: String, required: true },
    photo: { type: String, required: false },
    general_students_average: { type: Number, default: 0 }, 
    users:[{
        type:Schema.Types.ObjectId,
        ref:"User"
    }],
    quarters:[{
        type:Schema.Types.ObjectId,
        ref:"Quarters"
    }],
    vacations:[{
        type:Schema.Types.ObjectId,
        ref:"Vacations"
    }],
    events :[{
        type:Schema.Types.ObjectId,
        ref:"Events"
    }],
}, {
    timestamps: true
});

module.exports = mongoose.model('SchoolYears', SchoolYearsSchema);



