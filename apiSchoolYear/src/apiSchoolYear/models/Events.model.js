const SchoolYears = require('../models/SchoolYears.model.js');
const mongoose = require('mongoose');Schema = mongoose.Schema;

const EventsSchema = mongoose.Schema({
    designation: { type: String, required: true },
    start_date: { type: String, required: true },
    end_date: { type: Date, required: false },
    description: { type: String, required: false },
    photo: { type: String, required: false },
    schoolYears:{
        type:Schema.Types.ObjectId,
        ref:"SchoolYears"
    },
}, {
    timestamps: true
});

module.exports = mongoose.model('Events', EventsSchema);


