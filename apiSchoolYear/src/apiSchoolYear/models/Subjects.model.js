const Quarters = require('../models/Quarters.model.js');
const Sessions = require('../models/Sessions.model.js');
const Tests = require('../models/Tests.model.js');
//const Students = require('../models/Students.model.js');
const mongoose = require('mongoose');Schema = mongoose.Schema;

const SubjectsSchema = mongoose.Schema({
    designation: { type: String, required: true },
    coefficient: { type: Number, required: true },
    photo: { type: String, required: false },
    subject_student_average: { type: Number, default: 0 }, 
    quarters:[{
        type:Schema.Types.ObjectId,
        ref:"Quarters"
    }],
    sessions:[{
        type:Schema.Types.ObjectId,
        ref:"Sessions"
    }],
    tests:[{
        type:Schema.Types.ObjectId,
        ref:"Tests"
    }],
    /*students:[{
        type:Schema.Types.ObjectId,
        ref:"Students"
    }],*/
}, {
    timestamps: true
});

module.exports = mongoose.model('Subjects', SubjectsSchema);


