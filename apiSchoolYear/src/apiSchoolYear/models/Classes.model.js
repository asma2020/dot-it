const Quarters = require('../models/Quarters.model.js');
const Schedules = require('../models/Schedules.model.js');
//const Students = require('../models/Students.model.js');
const mongoose = require('mongoose');Schema = mongoose.Schema;

const ClassesSchema = mongoose.Schema({
    designation: { type: String, required: true },
    student_numbers: { type: Number, required: true },
    division: { type: String, required: false },
    quarters:[{
        type:Schema.Types.ObjectId,
        ref:"Quarters"
    }],
    schedules:[{
        type:Schema.Types.ObjectId,
        ref:"Schedules"
    }],
    /*students:[{
        type:Schema.Types.ObjectId,
        ref:"Students"
    }],*/
}, {
    timestamps: true
});

module.exports = mongoose.model('Classes', ClassesSchema);


