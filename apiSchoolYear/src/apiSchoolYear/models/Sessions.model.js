const Schedules = require('../models/Schedules.model.js');
//const Students = require('../models/Students.model.js');
const Subjects = require('../models/Subjects.model.js');
const mongoose = require('mongoose');Schema = mongoose.Schema;

const SessionsSchema = mongoose.Schema({
    designation: { type: String, required: true },
    salle: { type: Number, required: true },
    schedules:[{
        type:Schema.Types.ObjectId,
        ref:"Schedules"
    }],
    /*students:[{
        type:Schema.Types.ObjectId,
        ref:"Students"
    }],*/
    subjects:[{
        type:Schema.Types.ObjectId,
        ref:"Subjects"
    }]
}, {
    timestamps: true
});

module.exports = mongoose.model('Sessions', SessionsSchema);
