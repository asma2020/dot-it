const SchoolYears = require('../models/SchoolYears.model.js');
const Subjects = require('../models/Subjects.model.js');
const Classes = require('../models/Classes.model.js');
//const Students = require('../models/Students.model.js');
const mongoose = require('mongoose');Schema = mongoose.Schema;

const QuartersSchema = mongoose.Schema({
    designation: { type: String, required: true },
    start_date: { type: String, required: true },
    end_date: { type: Date, required: false },
    description: { type: String, required: false },
    student_quarterly_average: { type: Number, default: 0 }, 
    schoolYears:[{
        type:Schema.Types.ObjectId,
        ref:"SchoolYears"
    }],
    subjects:[{
        type:Schema.Types.ObjectId,
        ref:"Subjects"
    }],
    classes:[{
        type:Schema.Types.ObjectId,
        ref:"Classes"
    }],
    /*students:[{
        type:Schema.Types.ObjectId,
        ref:"Students"
    }]*/
}, {
    timestamps: true
});

module.exports = mongoose.model('Quarters', QuartersSchema);


