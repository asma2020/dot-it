const SchoolYears = require('../models/SchoolYears.model.js');
const mongoose = require('mongoose');Schema = mongoose.Schema;

const VacationsSchema = mongoose.Schema({
    designation: { type: String, required: true },
    start_date: { type: Date, required: true },
    end_date: { type: Date, required: true },
    type: { type: String, required: true },
    photo: { type: String, required: false },
    schoolYears:[{
        type:Schema.Types.ObjectId,
        ref:"SchoolYears"
    }],
}, {
    timestamps: true
});

module.exports = mongoose.model('Vacations', VacationsSchema);

