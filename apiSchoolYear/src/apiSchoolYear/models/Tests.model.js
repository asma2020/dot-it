const Subjects = require('../models/Subjects.model.js');
//const Students = require('../models/Students.model.js');
const mongoose = require('mongoose');Schema = mongoose.Schema;

const TestsSchema = mongoose.Schema({
    name: { type: String, required: true },
    type: { type: String, required: true },
    test_note: { type: Number, required: true },
    test_coefficient: { type: Number, required: true },
    subjects:[{
        type:Schema.Types.ObjectId,
        ref:"Subjects"
    }],
    /*students:[{
        type:Schema.Types.ObjectId,
        ref:"Students"
    }]*/
}, {
    timestamps: true
});

module.exports = mongoose.model('Tests', TestsSchema);


