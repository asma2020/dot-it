const express = require("express");

const app = express();
//const mongodb = require('mongodb');
const bodyParser = require('body-parser');
const morgan = require("morgan");
//const extracurricularRoutes = require("./apiExtracurricular/controllers/extracurricular");

//app.use(express.json());
//app.use(express.urlencoded({ extended: true }));

//Setup Cross Origin
app.use(require("cors")());
//Upload Image
app.use(bodyParser.json())
app.use(morgan("dev"));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

//Bring in the apiSchool routes
//app.use("/extracurriculars",require("./apiExtracurricular/routes/extracurricular.routes.js"));
app.use("/schoolYears",require("./apiSchoolYear/routes/schoolYear.routes.js"));
app.use("/events",require("./apiSchoolYear/routes/event.routes.js"));
app.use("/vacations",require("./apiSchoolYear/routes/vacation.routes.js"));
app.use("/quarters",require("./apiSchoolYear/routes/Quarter.routes.js"));
app.use("/subjects",require("./apiSchoolYear/routes/Subject.routes.js"));
app.use("/tests",require("./apiSchoolYear/routes/Test.routes.js"));
//app.use("/students",require("./apiSchoolYear/routes/Student.routes.js"));
app.use("/classes",require("./apiSchoolYear/routes/Classe.routes.js"));
app.use("/classes",require("./apiSchoolYear/routes/Classe.routes.js"));
app.use("/sessions",require("./apiSchoolYear/routes/Session.routes.js"));
app.use("/schedules",require("./apiSchoolYear/routes/Schedule.routes.js"));
//Require Chat routes
/*app.use("/user", require("./routes/user"));
app.use("/chatroom", require("./routes/chatroom"));
//Bring in the apiUser routes
app.use("/parents",require("./apiUser/routes/Parent.routes.js"));
//Bring in the apiExtracurricular routes
app.use("/comments",require("./apiExtracurricular/routes/comment.routes.js"));
app.use("/extracurriculars",require("./apiExtracurricular/routes/extracurricular.routes.js"));
//Bring in the apiAbsence routes
app.use("/studentAbsences",require("./apiAbsence/routes/StudentAbsence.routes.js"));
app.use("/teacherAbsences",require("./apiAbsence/routes/TeacherAbsence.routes.js"));
//app.use("/extracurriculars",require("./apiExtracurricular/routes/extracurricular.routes.js"));
//Setup Error Handlers*/

//Creating the event request handler (to handle the events published by the event-bus)
app.post('/events', (req, res) => {
  console.log('Received schoolYear Event', req.body.type);
  res.send({});
});

module.exports = app;
